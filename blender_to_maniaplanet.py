import tkinter, re, math, json

def create_input(master, text):
    input_label = tkinter.Label(master, text=text)
    input_entry = tkinter.Entry(master, width=15)

    input_label.grid(row=create_input.line, column=create_input.col, sticky="e")
    input_entry.grid(row=create_input.line, column=create_input.col+1)
    create_input.line += 1

    return input_entry

def create_input_category(heading, input_config):
    #Create the heading
    category_label = tkinter.Label(window, text=heading, font="Helvetica 10 bold")
    category_label.grid(row=create_input.line, column=create_input.col, columnspan=2)
    create_input.line += 1

    #create and return the inputs for later access
    input_category = dict()
    for setting, default_value in input_config.items():
        input_category[setting] = create_input(window, setting)
        input_category[setting].insert(0, default_value)

    return input_category

def convert_loc(loc):
    global gui
    # We divide by 2 because cube sizes in blender are mesured as radius
    full_scale = config["maniaplanet"]["Size"] / config["blender"]["Size"] / 2
    
    #This array contains the mappings for... erm yeah 
    mapping = [(0, "X", 1, "Y"), (1, "Y", 2, "Z"), (2, "Z", 0, "X")]

    ret = [0, 0, 0]
    for m_i, m, b_i, b in mapping:
        offset_maniaplanet = (loc[b_i] - config["blender"][b]) * full_scale
        ret[m_i] = offset_maniaplanet + config["maniaplanet"][m]
    return ret

def get_settings():
    settings = dict()
    for category in gui.keys():
        settings[category] = dict()
        for setting, entry in gui[category].items():
            settings[category][setting] = float(entry.get())
    
    return settings

def convert_file():
    global config
    config = get_settings()

    data_stage_0 = []
    with open("input/input.py", "r") as f:
        data_stage_0 = f.read().split("# ")

    #This regex will match something that looks like:
    #  new frame ...
    #  obj.location = (coordinates)             <--- 1st group
    #  obj.scale ... 
    #  obj.rotation = (coordinates)             <--- 2nd group
    #  ...
    reg_ex=r"new frame.*obj\.location = ([\-\d\.\s,]*)\nobj\.scale.*obj\.rotation_euler = ([-\d\.\s,]*)\n.*"

    with open("input/output.txt", "w") as out:
        for frame_data in data_stage_0:
            match = re.fullmatch(reg_ex, frame_data, re.S)
            if match != None:
                #get the coordinate triples and pack them in arrays
                loc = re.split(r",\s", match.group(1))
                rot = re.split(r",\s", match.group(2))

                #convert the strings into floats
                loc = list(map(float, loc))
                rot = list(map(float, rot))
                
                loc = convert_loc(loc)

                rot = list(map(lambda x: x / math.pi * 180, rot))
                rot[0] = 90 - rot[0]
                rot[2] = 90 + rot[2]
                rot[1], rot[2] = rot[2] % 360, rot[1]

                first_c = True
                for coord in loc + rot:
                    if(first_c):
                        first_c = False
                        out.write(str(round(coord, 3)))
                    else:
                        out.write(", " + str(round(coord, 3)))
                out.write("\n")


window = tkinter.Tk()

#Load configuration
conf_file = "config.json"
with open(conf_file) as json_data_file:
    config = json.load(json_data_file)
print("Loaded Configuration File ", conf_file, ":")
print(json.dumps(config, indent=2))

#Blender Settings
gui = dict()
create_input.line = 0
create_input.col = 0
gui["blender"] = create_input_category("Blender Settings", config["blender"])

max_line = create_input.line        #Backup the number of lines used by Blender Settings

#Maniplanet Settings
create_input.line = 0
create_input.col = 2
gui["maniaplanet"] = create_input_category("Maniaplanet Settings", config["maniaplanet"])

#General Settings
create_input.line = max(max_line, create_input.line)
create_input.col = 0
gui["general"] = create_input_category("General Settings", config["general"])

#OK and Save Config Button
button_ok = tkinter.Button(window, text="Start file conversion", bg="green", command=convert_file)
button_ok.grid(row=11, column=0, columnspan=3, sticky="WE")

button_save = tkinter.Button(window, text="to be added...")
button_save.grid(row=11, column=3, sticky="WE")

window.mainloop()