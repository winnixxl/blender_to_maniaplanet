^!j::
MsgBox, "Press F7 to interupt the Script"
;;;;;;  SETTINGS  ;;;;;;

start_time := 0				; times are in 1/100s of a second
end_time := 1300
fps := 30					; fps of original video 
keyframe_intervall := 100  	; Should be 6 or more
lens_angle := 31			; You find this in your blender export (line 7)

;;;;  END SETTINGS  ;;;;


CoordMode, Mouse, Client
CoordMode, Pixel, Client

; Open the advanced menu if necessary
PixelGetColor, targetColor, 100, 460
PixelGetColor, bgColor, 190, 460
if targetColor = %bgColor%
{
	Click, 230, 240
}

; set FOV on first and last Frame
Click, 170, 345
Send, {Del 10}%lens_angle%
Click, 170, 400
Send, {NumpadMult}
Click, 170, 345
Send, {Del 10}%lens_angle%
Click, 170, 400
Send, {NumpadDiv}




current_time := start_time
Loop
{	
	lineNumber := current_time * fps / 100 + 1
	FileReadLine, args, input\output.txt, %lineNumber%
	array := StrSplit(args, ",")

	Loop % array.MaxIndex()
	{
		x := 60 + 110 * Floor((A_Index - 1) / 3)
		y := 275 + 20 * Mod((A_Index - 1), 3)
		value := array[A_Index]
		
		Click, %x%, %y%
		SendInput, {Del 10}
		SendInput, %value%
	}

	current_time := current_time + keyframe_intervall

	PixelGetColor, errorBox, 1100, 475
	if current_time > %end_time%
		Break
	if errorBox = 0x442B04
		Break
	
	sec :=  SubStr("00" current_time // 100, -1)
	csec := Substr("00" Mod(current_time, 100), -1)

	Click, 835, 675
	SendInput, {BS 10}
	Sleep, 30
	SendInput, 0:%sec%.%csec%
	Click, 900, 675
	Click, 1370, 675
}

Progress, Off
return

F7::ExitApp