import bpy
cameras = {}
scene = bpy.context.scene
frame = scene.frame_current - 1

data = bpy.data.cameras.new('Camera')
data.lens = 30.60024642944336
data.shift_x = 0.0
data.shift_y = 0.0
data.dof_distance = 0.0
data.clip_start = 0.10000000149011612
data.clip_end = 100.0
data.draw_size = 0.5
obj = bpy.data.objects.new('Camera', data)
obj.hide_render = False
scene.objects.link(obj)
cameras['Camera'] = obj

# new frame
scene.frame_set(400 + frame)
obj = cameras['Camera']
obj.location = 1.0573534965515137, -1.2327793836593628, 0.5587787628173828
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.5140380859375, -0.0716809332370758, 1.6658694744110107
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(401 + frame)
obj = cameras['Camera']
obj.location = 1.0527431964874268, -1.242183804512024, 0.5550161600112915
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.5104262828826904, -0.0711028054356575, 1.696907639503479
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(402 + frame)
obj = cameras['Camera']
obj.location = 1.0481328964233398, -1.251588225364685, 0.5512535572052002
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.5068342685699463, -0.07041339576244354, 1.727942705154419
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(403 + frame)
obj = cameras['Camera']
obj.location = 1.043522596359253, -1.2609926462173462, 0.5474909543991089
obj.scale = 1.000000238418579, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.5032660961151123, -0.06961311399936676, 1.7589718103408813
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(404 + frame)
obj = cameras['Camera']
obj.location = 1.0389121770858765, -1.2703971862792969, 0.5437283515930176
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.4997241497039795, -0.06870274245738983, 1.7899962663650513
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(405 + frame)
obj = cameras['Camera']
obj.location = 1.0343018770217896, -1.279801607131958, 0.5399657487869263
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.4962128400802612, -0.06768316775560379, 1.8210164308547974
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(406 + frame)
obj = cameras['Camera']
obj.location = 1.0296915769577026, -1.2892060279846191, 0.5362032055854797
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.4927352666854858, -0.06655548512935638, 1.8520292043685913
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(407 + frame)
obj = cameras['Camera']
obj.location = 1.0250812768936157, -1.2986104488372803, 0.5324406027793884
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.4892945289611816, -0.06532088667154312, 1.88303542137146
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(408 + frame)
obj = cameras['Camera']
obj.location = 1.0204709768295288, -1.3080148696899414, 0.5286779999732971
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.485893964767456, -0.06398020684719086, 1.9140348434448242
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(409 + frame)
obj = cameras['Camera']
obj.location = 1.015860676765442, -1.3174192905426025, 0.5249153971672058
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.482537031173706, -0.06253520399332047, 1.9450267553329468
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(410 + frame)
obj = cameras['Camera']
obj.location = 1.011250376701355, -1.3268237113952637, 0.5211528539657593
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.4792269468307495, -0.06098715215921402, 1.9760103225708008
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(411 + frame)
obj = cameras['Camera']
obj.location = 1.006640076637268, -1.3362282514572144, 0.517390251159668
obj.scale = 1.000000238418579, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.4759668111801147, -0.05933747440576553, 2.006986141204834
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(412 + frame)
obj = cameras['Camera']
obj.location = 1.0020297765731812, -1.3456326723098755, 0.5136276483535767
obj.scale = 1.000000238418579, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.4727598428726196, -0.05758770555257797, 2.0379526615142822
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(413 + frame)
obj = cameras['Camera']
obj.location = 0.9974194169044495, -1.3550370931625366, 0.5098650455474854
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.4696087837219238, -0.05573973059654236, 2.068910598754883
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(414 + frame)
obj = cameras['Camera']
obj.location = 0.9928091168403625, -1.3644415140151978, 0.506102442741394
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.4665168523788452, -0.05379530042409897, 2.0998589992523193
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(415 + frame)
obj = cameras['Camera']
obj.location = 0.9881988167762756, -1.3738459348678589, 0.5023398399353027
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.463487148284912, -0.051756277680397034, 2.130797863006592
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(416 + frame)
obj = cameras['Camera']
obj.location = 0.9835885167121887, -1.38325035572052, 0.4985772669315338
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998211860657
obj.rotation_euler = 1.460522174835205, -0.04962477833032608, 2.161726951599121
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(417 + frame)
obj = cameras['Camera']
obj.location = 0.9789782166481018, -1.3926548957824707, 0.4948146939277649
obj.scale = 1.000000238418579, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.4576250314712524, -0.047402627766132355, 2.1926467418670654
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(418 + frame)
obj = cameras['Camera']
obj.location = 0.9743678569793701, -1.4020593166351318, 0.4910520911216736
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998211860657
obj.rotation_euler = 1.4547983407974243, -0.045091934502124786, 2.2235562801361084
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(419 + frame)
obj = cameras['Camera']
obj.location = 0.9697575569152832, -1.411463737487793, 0.48728951811790466
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.4520447254180908, -0.04269544035196304, 2.2544562816619873
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(420 + frame)
obj = cameras['Camera']
obj.location = 0.9651472568511963, -1.420868158340454, 0.48352691531181335
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.4493669271469116, -0.04021487012505531, 2.285346269607544
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(421 + frame)
obj = cameras['Camera']
obj.location = 0.9605369567871094, -1.4302725791931152, 0.47976431250572205
obj.scale = 1.000000238418579, 0.9999999403953552, 0.9999999403953552
obj.rotation_euler = 1.4467674493789673, -0.037653062492609024, 2.3162267208099365
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(422 + frame)
obj = cameras['Camera']
obj.location = 0.9572619199752808, -1.4350069761276245, 0.47797471284866333
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999999403953552
obj.rotation_euler = 1.4447576999664307, -0.03693031519651413, 2.346496343612671
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(423 + frame)
obj = cameras['Camera']
obj.location = 0.9556305408477783, -1.4392694234848022, 0.47613900899887085
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.4427534341812134, -0.03734211623668671, 2.3738884925842285
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(424 + frame)
obj = cameras['Camera']
obj.location = 0.9557516574859619, -1.4438635110855103, 0.4737696945667267
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999999403953552
obj.rotation_euler = 1.440328598022461, -0.0363558828830719, 2.400956630706787
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(425 + frame)
obj = cameras['Camera']
obj.location = 0.973171055316925, -1.4247599840164185, 0.48373934626579285
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.438063621520996, -0.04760478436946869, 2.4298176765441895
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(426 + frame)
obj = cameras['Camera']
obj.location = 0.9696224331855774, -1.4347196817398071, 0.48022204637527466
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.4346935749053955, -0.04590689390897751, 2.45806622505188
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(427 + frame)
obj = cameras['Camera']
obj.location = 0.9638407826423645, -1.4419244527816772, 0.4790673851966858
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.431019902229309, -0.04428885504603386, 2.486177444458008
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(428 + frame)
obj = cameras['Camera']
obj.location = 0.9605301022529602, -1.4453986883163452, 0.4774336516857147
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998211860657
obj.rotation_euler = 1.4278655052185059, -0.042631879448890686, 2.51021671295166
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(429 + frame)
obj = cameras['Camera']
obj.location = 0.9640936255455017, -1.4404382705688477, 0.47953784465789795
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.424808382987976, -0.04390570521354675, 2.530848741531372
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(430 + frame)
obj = cameras['Camera']
obj.location = 0.9635703563690186, -1.4438141584396362, 0.4778280258178711
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.421999216079712, -0.043760426342487335, 2.550217866897583
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(431 + frame)
obj = cameras['Camera']
obj.location = 0.9625231623649597, -1.4457674026489258, 0.47642451524734497
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998211860657
obj.rotation_euler = 1.4190081357955933, -0.04301659017801285, 2.5689969062805176
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(432 + frame)
obj = cameras['Camera']
obj.location = 0.9628844857215881, -1.4482101202011108, 0.4752589762210846
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.4157299995422363, -0.04216831177473068, 2.5870590209960938
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(433 + frame)
obj = cameras['Camera']
obj.location = 0.9613392949104309, -1.4488646984100342, 0.4743799567222595
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.4122520685195923, -0.04188930243253708, 2.6054673194885254
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(434 + frame)
obj = cameras['Camera']
obj.location = 0.9645479321479797, -1.4386060237884521, 0.4787293076515198
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.4077718257904053, -0.04397710785269737, 2.6208972930908203
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(435 + frame)
obj = cameras['Camera']
obj.location = 0.962093710899353, -1.4370814561843872, 0.4788171350955963
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.4041407108306885, -0.044327571988105774, 2.6328771114349365
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(436 + frame)
obj = cameras['Camera']
obj.location = 0.9611837267875671, -1.4398592710494995, 0.4777497947216034
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.400938868522644, -0.045801661908626556, 2.6418416500091553
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(437 + frame)
obj = cameras['Camera']
obj.location = 0.9607956409454346, -1.4391741752624512, 0.47740593552589417
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3979499340057373, -0.04664813354611397, 2.651218891143799
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(438 + frame)
obj = cameras['Camera']
obj.location = 0.9610628485679626, -1.4425023794174194, 0.47595399618148804
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.395019769668579, -0.04784630984067917, 2.6606664657592773
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(439 + frame)
obj = cameras['Camera']
obj.location = 0.9608902335166931, -1.4433790445327759, 0.4752871096134186
obj.scale = 1.000000238418579, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3918863534927368, -0.04926302656531334, 2.6716556549072266
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(440 + frame)
obj = cameras['Camera']
obj.location = 0.9573718905448914, -1.4456837177276611, 0.4745353162288666
obj.scale = 1.000000238418579, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.388755440711975, -0.05015067756175995, 2.6812546253204346
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(441 + frame)
obj = cameras['Camera']
obj.location = 0.9570972919464111, -1.4463781118392944, 0.4744831919670105
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3860512971878052, -0.051893483847379684, 2.6876792907714844
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(442 + frame)
obj = cameras['Camera']
obj.location = 0.9577234983444214, -1.4484026432037354, 0.4737558662891388
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3837831020355225, -0.05347822606563568, 2.6939148902893066
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(443 + frame)
obj = cameras['Camera']
obj.location = 0.9580782055854797, -1.4501014947891235, 0.4733595550060272
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998211860657
obj.rotation_euler = 1.3814451694488525, -0.05437876656651497, 2.701427936553955
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(444 + frame)
obj = cameras['Camera']
obj.location = 0.9575973749160767, -1.451387882232666, 0.47270575165748596
obj.scale = 1.000000238418579, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3793946504592896, -0.05504877120256424, 2.708982467651367
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(445 + frame)
obj = cameras['Camera']
obj.location = 0.9570640325546265, -1.4525889158248901, 0.47215914726257324
obj.scale = 1.000000238418579, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3772554397583008, -0.055641528218984604, 2.716481924057007
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(446 + frame)
obj = cameras['Camera']
obj.location = 0.9562116861343384, -1.4535144567489624, 0.4719177186489105
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3751716613769531, -0.055425699800252914, 2.7235629558563232
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(447 + frame)
obj = cameras['Camera']
obj.location = 0.9550673961639404, -1.454437017440796, 0.47172775864601135
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3733134269714355, -0.05569932237267494, 2.7296531200408936
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(448 + frame)
obj = cameras['Camera']
obj.location = 0.9546871185302734, -1.4550420045852661, 0.4716234803199768
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3719426393508911, -0.055879440158605576, 2.734341621398926
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(449 + frame)
obj = cameras['Camera']
obj.location = 0.955418586730957, -1.4558985233306885, 0.4712950587272644
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.370888352394104, -0.05585143342614174, 2.738452434539795
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(450 + frame)
obj = cameras['Camera']
obj.location = 0.9553496837615967, -1.4570164680480957, 0.47109371423721313
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3699240684509277, -0.055674489587545395, 2.743732213973999
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(451 + frame)
obj = cameras['Camera']
obj.location = 0.9549548625946045, -1.4576756954193115, 0.4709809422492981
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998211860657
obj.rotation_euler = 1.3689682483673096, -0.055276647210121155, 2.7489237785339355
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(452 + frame)
obj = cameras['Camera']
obj.location = 0.956203043460846, -1.4560734033584595, 0.4716954827308655
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3679639101028442, -0.05523085221648216, 2.7539451122283936
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(453 + frame)
obj = cameras['Camera']
obj.location = 0.9556459784507751, -1.456831693649292, 0.47142869234085083
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3670547008514404, -0.05417269095778465, 2.7590410709381104
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(454 + frame)
obj = cameras['Camera']
obj.location = 0.9553582668304443, -1.4574692249298096, 0.4713771641254425
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.3662739992141724, -0.05251739174127579, 2.7634050846099854
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(455 + frame)
obj = cameras['Camera']
obj.location = 0.9549190402030945, -1.4581242799758911, 0.47093334794044495
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.365759015083313, -0.05164080113172531, 2.7674753665924072
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(456 + frame)
obj = cameras['Camera']
obj.location = 0.955037534236908, -1.4583662748336792, 0.4708290994167328
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3651477098464966, -0.05131622776389122, 2.771402597427368
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(457 + frame)
obj = cameras['Camera']
obj.location = 0.9549063444137573, -1.4592218399047852, 0.4705803394317627
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3645399808883667, -0.05034169927239418, 2.775094985961914
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(458 + frame)
obj = cameras['Camera']
obj.location = 0.9543551206588745, -1.4605082273483276, 0.47026047110557556
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3640767335891724, -0.04971122369170189, 2.77875018119812
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(459 + frame)
obj = cameras['Camera']
obj.location = 0.9542558193206787, -1.461540937423706, 0.46980345249176025
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3639293909072876, -0.049270931631326675, 2.7821567058563232
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(460 + frame)
obj = cameras['Camera']
obj.location = 0.954925537109375, -1.460225224494934, 0.47059205174446106
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3635467290878296, -0.048677753657102585, 2.7855916023254395
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(461 + frame)
obj = cameras['Camera']
obj.location = 0.9545130133628845, -1.4613726139068604, 0.4704197645187378
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.363369345664978, -0.04814459756016731, 2.788975238800049
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(462 + frame)
obj = cameras['Camera']
obj.location = 0.9543401598930359, -1.4626796245574951, 0.4701565206050873
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998211860657
obj.rotation_euler = 1.363394021987915, -0.04723074287176132, 2.791994571685791
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(463 + frame)
obj = cameras['Camera']
obj.location = 0.954021692276001, -1.4635074138641357, 0.46991053223609924
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.3635101318359375, -0.04592324048280716, 2.7949254512786865
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(464 + frame)
obj = cameras['Camera']
obj.location = 0.9541832208633423, -1.4638639688491821, 0.46976718306541443
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3636796474456787, -0.04465187340974808, 2.79767107963562
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(465 + frame)
obj = cameras['Camera']
obj.location = 0.9536810517311096, -1.464604139328003, 0.4694981873035431
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3639495372772217, -0.04324549809098244, 2.8005385398864746
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(466 + frame)
obj = cameras['Camera']
obj.location = 0.9533360600471497, -1.4651165008544922, 0.4694640040397644
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3641626834869385, -0.04176434129476547, 2.803257465362549
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(467 + frame)
obj = cameras['Camera']
obj.location = 0.9534572958946228, -1.4656327962875366, 0.46951034665107727
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3644009828567505, -0.040127966552972794, 2.8057494163513184
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(468 + frame)
obj = cameras['Camera']
obj.location = 0.953009843826294, -1.4665114879608154, 0.46909287571907043
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3649606704711914, -0.039473388344049454, 2.808483839035034
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(469 + frame)
obj = cameras['Camera']
obj.location = 0.9528481364250183, -1.4677790403366089, 0.4688817858695984
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.365397572517395, -0.03993040323257446, 2.811086416244507
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(470 + frame)
obj = cameras['Camera']
obj.location = 0.9522518515586853, -1.4686763286590576, 0.46859505772590637
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.3658759593963623, -0.03980911895632744, 2.813601016998291
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(471 + frame)
obj = cameras['Camera']
obj.location = 0.9521691203117371, -1.469239592552185, 0.4684094786643982
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.3663731813430786, -0.03961663693189621, 2.8158555030822754
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(472 + frame)
obj = cameras['Camera']
obj.location = 0.952341616153717, -1.4692106246948242, 0.4683021903038025
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.366866111755371, -0.03915909305214882, 2.81805419921875
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(473 + frame)
obj = cameras['Camera']
obj.location = 0.9521421790122986, -1.4689686298370361, 0.46840307116508484
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998211860657
obj.rotation_euler = 1.3671079874038696, -0.038413576781749725, 2.820478916168213
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(474 + frame)
obj = cameras['Camera']
obj.location = 0.9521571397781372, -1.4691483974456787, 0.4683588147163391
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3673611879348755, -0.03810601308941841, 2.8228371143341064
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(475 + frame)
obj = cameras['Camera']
obj.location = 0.9515962600708008, -1.4698904752731323, 0.46810394525527954
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.367640495300293, -0.03699042275547981, 2.82521915435791
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(476 + frame)
obj = cameras['Camera']
obj.location = 0.9519788026809692, -1.4705125093460083, 0.46803200244903564
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3679250478744507, -0.03568397834897041, 2.8270864486694336
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(477 + frame)
obj = cameras['Camera']
obj.location = 0.9520930647850037, -1.4701218605041504, 0.46832719445228577
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3680285215377808, -0.03431712090969086, 2.8292248249053955
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(478 + frame)
obj = cameras['Camera']
obj.location = 0.9518540501594543, -1.4703383445739746, 0.46827974915504456
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3681995868682861, -0.03323694318532944, 2.8315446376800537
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(479 + frame)
obj = cameras['Camera']
obj.location = 0.9514528512954712, -1.4714096784591675, 0.46797850728034973
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998211860657
obj.rotation_euler = 1.3684544563293457, -0.03237539157271385, 2.8337554931640625
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(480 + frame)
obj = cameras['Camera']
obj.location = 0.9513577818870544, -1.4721362590789795, 0.4677971303462982
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.368747591972351, -0.03159140422940254, 2.8356659412384033
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(481 + frame)
obj = cameras['Camera']
obj.location = 0.9508885145187378, -1.4726557731628418, 0.4674759805202484
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.3690563440322876, -0.03127056732773781, 2.8376810550689697
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(482 + frame)
obj = cameras['Camera']
obj.location = 0.9504613280296326, -1.4732420444488525, 0.4672996699810028
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.3692716360092163, -0.03160165622830391, 2.8396363258361816
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(483 + frame)
obj = cameras['Camera']
obj.location = 0.9500856995582581, -1.4735890626907349, 0.46740567684173584
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3693734407424927, -0.03219306096434593, 2.8413898944854736
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(484 + frame)
obj = cameras['Camera']
obj.location = 0.9496309757232666, -1.4742236137390137, 0.4670097231864929
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3695833683013916, -0.03289778158068657, 2.8431169986724854
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(485 + frame)
obj = cameras['Camera']
obj.location = 0.9491323232650757, -1.4746891260147095, 0.46675893664360046
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.369726538658142, -0.032150086015462875, 2.844438314437866
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(486 + frame)
obj = cameras['Camera']
obj.location = 0.9490106701850891, -1.4752153158187866, 0.4665822684764862
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3700191974639893, -0.030210796743631363, 2.845236301422119
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(487 + frame)
obj = cameras['Camera']
obj.location = 0.9483023881912231, -1.4755895137786865, 0.4662715196609497
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3701996803283691, -0.028727825731039047, 2.8460447788238525
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(488 + frame)
obj = cameras['Camera']
obj.location = 0.947891354560852, -1.4760106801986694, 0.46601855754852295
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.37040376663208, -0.02770443819463253, 2.846567392349243
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(489 + frame)
obj = cameras['Camera']
obj.location = 0.9474649429321289, -1.476447343826294, 0.46580466628074646
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3705651760101318, -0.026681378483772278, 2.846895694732666
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(490 + frame)
obj = cameras['Camera']
obj.location = 0.9470284581184387, -1.4766526222229004, 0.4657256007194519
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3706483840942383, -0.025840867310762405, 2.847201347351074
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(491 + frame)
obj = cameras['Camera']
obj.location = 0.9471371173858643, -1.4761179685592651, 0.46612584590911865
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3706632852554321, -0.025439491495490074, 2.8475165367126465
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(492 + frame)
obj = cameras['Camera']
obj.location = 0.9465731382369995, -1.4759292602539062, 0.4661412537097931
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3707185983657837, -0.025780770927667618, 2.8482537269592285
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(493 + frame)
obj = cameras['Camera']
obj.location = 0.9462818503379822, -1.4762753248214722, 0.4660393297672272
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3706685304641724, -0.02662532590329647, 2.8489060401916504
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(494 + frame)
obj = cameras['Camera']
obj.location = 0.9457122087478638, -1.4765974283218384, 0.465638667345047
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.37074875831604, -0.026617716997861862, 2.8492205142974854
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(495 + frame)
obj = cameras['Camera']
obj.location = 0.9452071189880371, -1.4770361185073853, 0.46552592515945435
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3707294464111328, -0.026083722710609436, 2.8493340015411377
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(496 + frame)
obj = cameras['Camera']
obj.location = 0.9450376033782959, -1.4770209789276123, 0.4656166434288025
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.370612621307373, -0.024722762405872345, 2.8491580486297607
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(497 + frame)
obj = cameras['Camera']
obj.location = 0.9448078274726868, -1.4770067930221558, 0.4654124677181244
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.370656967163086, -0.023724544793367386, 2.849074125289917
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(498 + frame)
obj = cameras['Camera']
obj.location = 0.9443803429603577, -1.4765876531600952, 0.4656379222869873
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3705785274505615, -0.02292017638683319, 2.8493475914001465
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(499 + frame)
obj = cameras['Camera']
obj.location = 0.9442762136459351, -1.4763576984405518, 0.46576985716819763
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3705037832260132, -0.02253679744899273, 2.849496364593506
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(500 + frame)
obj = cameras['Camera']
obj.location = 0.9436257481575012, -1.4761184453964233, 0.4658835530281067
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998211860657
obj.rotation_euler = 1.3702843189239502, -0.022888856008648872, 2.8501288890838623
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(501 + frame)
obj = cameras['Camera']
obj.location = 0.9431115388870239, -1.4760061502456665, 0.46593964099884033
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3701512813568115, -0.023049330338835716, 2.8505938053131104
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(502 + frame)
obj = cameras['Camera']
obj.location = 0.942417323589325, -1.4765442609786987, 0.46558570861816406
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3700745105743408, -0.02309085801243782, 2.850836753845215
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(503 + frame)
obj = cameras['Camera']
obj.location = 0.9418029189109802, -1.4765846729278564, 0.46537357568740845
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.370003581047058, -0.023058608174324036, 2.8511180877685547
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(504 + frame)
obj = cameras['Camera']
obj.location = 0.941268801689148, -1.4762097597122192, 0.46563002467155457
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3698508739471436, -0.021783707663416862, 2.851223945617676
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(505 + frame)
obj = cameras['Camera']
obj.location = 0.9408640265464783, -1.4753766059875488, 0.46579012274742126
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3697004318237305, -0.020500466227531433, 2.8515021800994873
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(506 + frame)
obj = cameras['Camera']
obj.location = 0.940514862537384, -1.4743531942367554, 0.46625521779060364
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3694379329681396, -0.019090166315436363, 2.851916790008545
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(507 + frame)
obj = cameras['Camera']
obj.location = 0.9396228194236755, -1.4742436408996582, 0.4660888612270355
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3692940473556519, -0.017748186364769936, 2.852459669113159
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(508 + frame)
obj = cameras['Camera']
obj.location = 0.9387611150741577, -1.4742141962051392, 0.46567201614379883
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3692370653152466, -0.01619843952357769, 2.8527095317840576
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(509 + frame)
obj = cameras['Camera']
obj.location = 0.9375265836715698, -1.4739161729812622, 0.46575742959976196
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.368914246559143, -0.014084815979003906, 2.853053092956543
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(510 + frame)
obj = cameras['Camera']
obj.location = 0.936881422996521, -1.473480224609375, 0.4659174680709839
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3686059713363647, -0.011797117069363594, 2.8532021045684814
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(511 + frame)
obj = cameras['Camera']
obj.location = 0.9353678226470947, -1.4733518362045288, 0.46614569425582886
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3681237697601318, -0.00913024041801691, 2.8538222312927246
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(512 + frame)
obj = cameras['Camera']
obj.location = 0.9344642758369446, -1.4729574918746948, 0.4663316309452057
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3679218292236328, -0.007198541425168514, 2.8543972969055176
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(513 + frame)
obj = cameras['Camera']
obj.location = 0.9331452250480652, -1.4737361669540405, 0.4660739302635193
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.3677427768707275, -0.006079686339944601, 2.855272054672241
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(514 + frame)
obj = cameras['Camera']
obj.location = 0.9318927526473999, -1.4737293720245361, 0.46585357189178467
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3676244020462036, -0.004781016148626804, 2.856203556060791
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(515 + frame)
obj = cameras['Camera']
obj.location = 0.9306365847587585, -1.4745292663574219, 0.4661027491092682
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3672387599945068, -0.004005044233053923, 2.857100009918213
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(516 + frame)
obj = cameras['Camera']
obj.location = 0.9294208288192749, -1.4743858575820923, 0.465884268283844
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.3671040534973145, -0.0027619723696261644, 2.8579633235931396
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(517 + frame)
obj = cameras['Camera']
obj.location = 0.9282026290893555, -1.474198579788208, 0.46602094173431396
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.3666517734527588, -0.0021990814711898565, 2.8589320182800293
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(518 + frame)
obj = cameras['Camera']
obj.location = 0.9269827008247375, -1.4745053052902222, 0.46555477380752563
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.365864634513855, -0.0007478083134628832, 2.8597967624664307
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(519 + frame)
obj = cameras['Camera']
obj.location = 0.9252711534500122, -1.4774694442749023, 0.4643647074699402
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3647464513778687, 0.000869039329700172, 2.8602333068847656
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(520 + frame)
obj = cameras['Camera']
obj.location = 0.9234570860862732, -1.4809809923171997, 0.46295708417892456
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3636868000030518, 0.001197865349240601, 2.860250949859619
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(521 + frame)
obj = cameras['Camera']
obj.location = 0.9217077493667603, -1.4836865663528442, 0.46241775155067444
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998211860657
obj.rotation_euler = 1.362768530845642, 0.001054180203936994, 2.8599953651428223
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(522 + frame)
obj = cameras['Camera']
obj.location = 0.9199747443199158, -1.4869755506515503, 0.46120893955230713
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.361928939819336, -0.00030468415934592485, 2.859421730041504
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(523 + frame)
obj = cameras['Camera']
obj.location = 0.9181360602378845, -1.4892953634262085, 0.4605289101600647
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3611764907836914, -0.0012896984117105603, 2.8584046363830566
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(524 + frame)
obj = cameras['Camera']
obj.location = 0.9169789552688599, -1.4910132884979248, 0.46078425645828247
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3606115579605103, -0.0009757223888300359, 2.856668710708618
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(525 + frame)
obj = cameras['Camera']
obj.location = 0.9159602522850037, -1.492238998413086, 0.4608990550041199
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999999403953552
obj.rotation_euler = 1.360136866569519, -0.0005173670360818505, 2.8553030490875244
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(526 + frame)
obj = cameras['Camera']
obj.location = 0.9144070148468018, -1.4934014081954956, 0.46063727140426636
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.3594763278961182, 0.0004891051794402301, 2.8544788360595703
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(527 + frame)
obj = cameras['Camera']
obj.location = 0.9127615690231323, -1.495991587638855, 0.46022143959999084
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.358391284942627, 0.0010989631991833448, 2.853797435760498
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(528 + frame)
obj = cameras['Camera']
obj.location = 0.9110794067382812, -1.4990423917770386, 0.45956605672836304
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.357361078262329, 0.0007202780689112842, 2.8532419204711914
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(529 + frame)
obj = cameras['Camera']
obj.location = 0.909307062625885, -1.5020524263381958, 0.459100604057312
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.356166958808899, -0.0012338138185441494, 2.8530802726745605
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(530 + frame)
obj = cameras['Camera']
obj.location = 0.9069673418998718, -1.5065770149230957, 0.4577069580554962
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.355282187461853, -0.0032899919897317886, 2.852707624435425
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(531 + frame)
obj = cameras['Camera']
obj.location = 0.9047161340713501, -1.5125586986541748, 0.45653995871543884
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.3543552160263062, -0.0064632948487997055, 2.851602554321289
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(532 + frame)
obj = cameras['Camera']
obj.location = 0.903290867805481, -1.517011284828186, 0.4560094177722931
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3543684482574463, -0.010534247383475304, 2.8500521183013916
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(533 + frame)
obj = cameras['Camera']
obj.location = 0.9012784957885742, -1.521031141281128, 0.4554181694984436
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.354983925819397, -0.014131888747215271, 2.848480701446533
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(534 + frame)
obj = cameras['Camera']
obj.location = 0.8995998501777649, -1.5256197452545166, 0.4549570083618164
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.355256199836731, -0.01730065420269966, 2.8466386795043945
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(535 + frame)
obj = cameras['Camera']
obj.location = 0.8983287215232849, -1.5294435024261475, 0.4555986225605011
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.3552289009094238, -0.01935289241373539, 2.844937562942505
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(536 + frame)
obj = cameras['Camera']
obj.location = 0.8981142044067383, -1.5313475131988525, 0.4571467339992523
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3558437824249268, -0.02105005644261837, 2.843865156173706
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(537 + frame)
obj = cameras['Camera']
obj.location = 0.8976989984512329, -1.5318019390106201, 0.4585929214954376
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.3569471836090088, -0.02151409722864628, 2.8439786434173584
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(538 + frame)
obj = cameras['Camera']
obj.location = 0.896253764629364, -1.5352308750152588, 0.4588436782360077
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.358011245727539, -0.02244700863957405, 2.84494686126709
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(539 + frame)
obj = cameras['Camera']
obj.location = 0.8953609466552734, -1.5373908281326294, 0.4591257870197296
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.358889102935791, -0.02227502502501011, 2.8458945751190186
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(540 + frame)
obj = cameras['Camera']
obj.location = 0.8943392038345337, -1.539279818534851, 0.45954418182373047
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.3594279289245605, -0.021443387493491173, 2.847144603729248
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(541 + frame)
obj = cameras['Camera']
obj.location = 0.8928991556167603, -1.5418238639831543, 0.46014437079429626
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3593629598617554, -0.01902025192975998, 2.8487117290496826
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(542 + frame)
obj = cameras['Camera']
obj.location = 0.8913507461547852, -1.5445077419281006, 0.4600945711135864
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998211860657
obj.rotation_euler = 1.35923171043396, -0.01612444967031479, 2.8504719734191895
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(543 + frame)
obj = cameras['Camera']
obj.location = 0.8896046280860901, -1.5471234321594238, 0.4600483179092407
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3587981462478638, -0.014545884914696217, 2.8525259494781494
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(544 + frame)
obj = cameras['Camera']
obj.location = 0.8873221278190613, -1.5506656169891357, 0.45954227447509766
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.3579483032226562, -0.01376854907721281, 2.854642391204834
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(545 + frame)
obj = cameras['Camera']
obj.location = 0.8847824335098267, -1.5544428825378418, 0.45880094170570374
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3566538095474243, -0.013835033401846886, 2.856576919555664
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(546 + frame)
obj = cameras['Camera']
obj.location = 0.8828407526016235, -1.557470440864563, 0.45863860845565796
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3549867868423462, -0.012212010100483894, 2.857887029647827
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(547 + frame)
obj = cameras['Camera']
obj.location = 0.8802071809768677, -1.5620226860046387, 0.4580782949924469
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3535833358764648, -0.009308958426117897, 2.858509063720703
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(548 + frame)
obj = cameras['Camera']
obj.location = 0.8784395456314087, -1.5659550428390503, 0.4578689932823181
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3522354364395142, -0.006158876698464155, 2.8584206104278564
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(549 + frame)
obj = cameras['Camera']
obj.location = 0.8767098188400269, -1.5692638158798218, 0.45812252163887024
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.351110577583313, -0.003820159938186407, 2.8584320545196533
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(550 + frame)
obj = cameras['Camera']
obj.location = 0.8751742243766785, -1.5719151496887207, 0.45865631103515625
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3502293825149536, -0.002865398768335581, 2.8588509559631348
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(551 + frame)
obj = cameras['Camera']
obj.location = 0.8731452226638794, -1.5749459266662598, 0.458787739276886
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3495875597000122, -0.0021471702493727207, 2.8596668243408203
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(552 + frame)
obj = cameras['Camera']
obj.location = 0.8713734149932861, -1.5772762298583984, 0.4589935839176178
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3490102291107178, -0.0011636074632406235, 2.8604657649993896
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(553 + frame)
obj = cameras['Camera']
obj.location = 0.8698225617408752, -1.5791800022125244, 0.4595400094985962
obj.scale = 1.000000238418579, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.3483786582946777, -0.00039536613621748984, 2.8616342544555664
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(554 + frame)
obj = cameras['Camera']
obj.location = 0.8682174682617188, -1.5812081098556519, 0.4602195918560028
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3478423357009888, 0.0004633379285223782, 2.863250494003296
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(555 + frame)
obj = cameras['Camera']
obj.location = 0.8662656545639038, -1.5831975936889648, 0.46050360798835754
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3475316762924194, 0.0008371834992431104, 2.865589141845703
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(556 + frame)
obj = cameras['Camera']
obj.location = 0.8642894625663757, -1.586124300956726, 0.46044427156448364
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3473892211914062, 0.0010717273689806461, 2.868150472640991
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(557 + frame)
obj = cameras['Camera']
obj.location = 0.8615361452102661, -1.5894142389297485, 0.4601675868034363
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3474266529083252, 0.0010673177894204855, 2.870812177658081
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(558 + frame)
obj = cameras['Camera']
obj.location = 0.8590612411499023, -1.5922987461090088, 0.45972710847854614
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3473858833312988, 0.00031804683385416865, 2.8736953735351562
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(559 + frame)
obj = cameras['Camera']
obj.location = 0.8565943837165833, -1.5943697690963745, 0.4598221182823181
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3468595743179321, -0.0007711590733379126, 2.8770334720611572
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(560 + frame)
obj = cameras['Camera']
obj.location = 0.8535212874412537, -1.5973711013793945, 0.45975831151008606
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.346114993095398, -0.0017009993316605687, 2.8811562061309814
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(561 + frame)
obj = cameras['Camera']
obj.location = 0.8508659601211548, -1.6011251211166382, 0.4596189856529236
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.3457401990890503, -0.001980839530006051, 2.8851022720336914
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(562 + frame)
obj = cameras['Camera']
obj.location = 0.8535019755363464, -1.6072126626968384, 0.45952269434928894
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3457738161087036, -0.0008460272219963372, 2.8915228843688965
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(563 + frame)
obj = cameras['Camera']
obj.location = 0.8506060838699341, -1.6111409664154053, 0.4594794511795044
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3468148708343506, -0.0005563143058679998, 2.9047107696533203
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(564 + frame)
obj = cameras['Camera']
obj.location = 0.8436440229415894, -1.6156420707702637, 0.45861732959747314
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3485580682754517, -0.00038823310751467943, 2.9150350093841553
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(565 + frame)
obj = cameras['Camera']
obj.location = 0.8453982472419739, -1.6214221715927124, 0.4573129415512085
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.3494863510131836, 0.0033843473065644503, 2.9240570068359375
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(566 + frame)
obj = cameras['Camera']
obj.location = 0.8426710367202759, -1.6229301691055298, 0.45583903789520264
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3503310680389404, 0.005698546301573515, 2.940859317779541
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(567 + frame)
obj = cameras['Camera']
obj.location = 0.8286911249160767, -1.6268911361694336, 0.457062691450119
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.347734808921814, 0.007005683146417141, 2.954780101776123
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(568 + frame)
obj = cameras['Camera']
obj.location = 0.8249519467353821, -1.6324127912521362, 0.4565694034099579
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3463919162750244, 0.0021325075067579746, 2.9588921070098877
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(569 + frame)
obj = cameras['Camera']
obj.location = 0.822623610496521, -1.6351747512817383, 0.4557725787162781
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.3456342220306396, -0.002395517425611615, 2.962074041366577
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(570 + frame)
obj = cameras['Camera']
obj.location = 0.8193932771682739, -1.6378023624420166, 0.4563071131706238
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3440309762954712, -0.0035191471688449383, 2.965301990509033
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(571 + frame)
obj = cameras['Camera']
obj.location = 0.8155896067619324, -1.641205072402954, 0.4564075767993927
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.342605471611023, -0.003458793507888913, 2.9690346717834473
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(572 + frame)
obj = cameras['Camera']
obj.location = 0.8120966553688049, -1.6443933248519897, 0.45630940794944763
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.3414084911346436, -0.0033125951886177063, 2.9720277786254883
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(573 + frame)
obj = cameras['Camera']
obj.location = 0.8089367747306824, -1.6483162641525269, 0.4553118944168091
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3407623767852783, -0.0046993643045425415, 2.973355531692505
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(574 + frame)
obj = cameras['Camera']
obj.location = 0.8062787055969238, -1.6520464420318604, 0.4544137120246887
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3398394584655762, -0.005230338778346777, 2.9730634689331055
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(575 + frame)
obj = cameras['Camera']
obj.location = 0.8032262921333313, -1.6554136276245117, 0.4542761743068695
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3388054370880127, -0.004536214284598827, 2.972439765930176
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(576 + frame)
obj = cameras['Camera']
obj.location = 0.7997883558273315, -1.658571481704712, 0.45430997014045715
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.3376197814941406, -0.004495946224778891, 2.9721548557281494
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(577 + frame)
obj = cameras['Camera']
obj.location = 0.7964876294136047, -1.6620044708251953, 0.4543199837207794
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.336230993270874, -0.004428735934197903, 2.9718880653381348
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(578 + frame)
obj = cameras['Camera']
obj.location = 0.7935221195220947, -1.665353775024414, 0.45405447483062744
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.33550226688385, -0.003801346058025956, 2.9709811210632324
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(579 + frame)
obj = cameras['Camera']
obj.location = 0.7906531095504761, -1.669223427772522, 0.4537797272205353
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3349276781082153, -0.00441244151443243, 2.9695518016815186
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(580 + frame)
obj = cameras['Camera']
obj.location = 0.7877122759819031, -1.672709345817566, 0.4537702202796936
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3347797393798828, -0.004824009723961353, 2.9677224159240723
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(581 + frame)
obj = cameras['Camera']
obj.location = 0.7843563556671143, -1.6763708591461182, 0.45348435640335083
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998211860657
obj.rotation_euler = 1.3348218202590942, -0.005335033871233463, 2.9661381244659424
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(582 + frame)
obj = cameras['Camera']
obj.location = 0.7811450958251953, -1.6800618171691895, 0.45345041155815125
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.334875464439392, -0.004869066644459963, 2.96423077583313
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(583 + frame)
obj = cameras['Camera']
obj.location = 0.7780756950378418, -1.6842656135559082, 0.4531744718551636
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3351014852523804, -0.00465876841917634, 2.962061882019043
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(584 + frame)
obj = cameras['Camera']
obj.location = 0.7747853994369507, -1.688301920890808, 0.4532599151134491
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3354198932647705, -0.005442138761281967, 2.960245370864868
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(585 + frame)
obj = cameras['Camera']
obj.location = 0.7713025808334351, -1.6922153234481812, 0.45336708426475525
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3358018398284912, -0.00740248104557395, 2.958911180496216
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(586 + frame)
obj = cameras['Camera']
obj.location = 0.7676612138748169, -1.6962239742279053, 0.45359620451927185
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998211860657
obj.rotation_euler = 1.335984230041504, -0.00969350803643465, 2.958353042602539
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(587 + frame)
obj = cameras['Camera']
obj.location = 0.7643957138061523, -1.699772596359253, 0.4537515342235565
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.3360073566436768, -0.012262259609997272, 2.958519220352173
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(588 + frame)
obj = cameras['Camera']
obj.location = 0.7611599564552307, -1.703040599822998, 0.4543209969997406
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.3356984853744507, -0.012861519120633602, 2.9591684341430664
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(589 + frame)
obj = cameras['Camera']
obj.location = 0.757481575012207, -1.7076127529144287, 0.4544386863708496
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3353302478790283, -0.011891122907400131, 2.9601616859436035
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(590 + frame)
obj = cameras['Camera']
obj.location = 0.754644513130188, -1.7118983268737793, 0.4550994634628296
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.334890604019165, -0.011439671739935875, 2.960916519165039
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(591 + frame)
obj = cameras['Camera']
obj.location = 0.7515288591384888, -1.716006875038147, 0.455576628446579
obj.scale = 1.000000238418579, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.33480966091156, -0.010658315382897854, 2.9621925354003906
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(592 + frame)
obj = cameras['Camera']
obj.location = 0.7482807636260986, -1.7190792560577393, 0.4570004343986511
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3343544006347656, -0.009667289443314075, 2.964538335800171
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(593 + frame)
obj = cameras['Camera']
obj.location = 0.7449079155921936, -1.7222799062728882, 0.45802566409111023
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3341832160949707, -0.00736762722954154, 2.967313766479492
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(594 + frame)
obj = cameras['Camera']
obj.location = 0.7411521673202515, -1.7270492315292358, 0.4584033489227295
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3343405723571777, -0.004334216006100178, 2.969843864440918
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(595 + frame)
obj = cameras['Camera']
obj.location = 0.7374260425567627, -1.731126070022583, 0.45833730697631836
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3347551822662354, -6.27648551017046e-05, 2.9717466831207275
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(596 + frame)
obj = cameras['Camera']
obj.location = 0.7342191934585571, -1.7344846725463867, 0.459174245595932
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3347002267837524, 0.005006593186408281, 2.9737868309020996
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(597 + frame)
obj = cameras['Camera']
obj.location = 0.7334821820259094, -1.7380528450012207, 0.4598536789417267
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3346110582351685, 0.011109559796750546, 2.9781153202056885
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(598 + frame)
obj = cameras['Camera']
obj.location = 0.7262438535690308, -1.7417092323303223, 0.4602377116680145
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3343499898910522, 0.014298257417976856, 2.984280824661255
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(599 + frame)
obj = cameras['Camera']
obj.location = 0.7224959135055542, -1.7456166744232178, 0.4604783058166504
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3341505527496338, 0.013728977181017399, 2.9877405166625977
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(600 + frame)
obj = cameras['Camera']
obj.location = 0.7187613248825073, -1.7488596439361572, 0.4605448544025421
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3341578245162964, 0.01089150458574295, 2.991307258605957
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(601 + frame)
obj = cameras['Camera']
obj.location = 0.7188185453414917, -1.7487502098083496, 0.460599422454834
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.334139347076416, 0.010842528194189072, 2.9913086891174316
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(602 + frame)
obj = cameras['Camera']
obj.location = 0.7148252129554749, -1.7526541948318481, 0.46013882756233215
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3340585231781006, 0.009132076986134052, 2.9943456649780273
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(603 + frame)
obj = cameras['Camera']
obj.location = 0.7106493711471558, -1.7566677331924438, 0.4594392776489258
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.33405601978302, 0.008921471424400806, 2.996349573135376
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(604 + frame)
obj = cameras['Camera']
obj.location = 0.7068971395492554, -1.7603366374969482, 0.4590122401714325
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.3337618112564087, 0.009221424348652363, 2.9976847171783447
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(605 + frame)
obj = cameras['Camera']
obj.location = 0.7030589580535889, -1.7636017799377441, 0.45883938670158386
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.3332611322402954, 0.009853779338300228, 2.999220609664917
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(606 + frame)
obj = cameras['Camera']
obj.location = 0.6987113952636719, -1.7661757469177246, 0.45948249101638794
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.332231879234314, 0.011005569249391556, 3.0019195079803467
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(607 + frame)
obj = cameras['Camera']
obj.location = 0.6946287155151367, -1.7697758674621582, 0.4595401883125305
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3314374685287476, 0.013323270715773106, 3.0053048133850098
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(608 + frame)
obj = cameras['Camera']
obj.location = 0.6903567910194397, -1.7738726139068604, 0.45966678857803345
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3309123516082764, 0.015702275559306145, 3.011380434036255
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(609 + frame)
obj = cameras['Camera']
obj.location = 0.6839525699615479, -1.7775925397872925, 0.4598628878593445
obj.scale = 1.000000238418579, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.3304177522659302, 0.014511427842080593, 3.0179567337036133
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(610 + frame)
obj = cameras['Camera']
obj.location = 0.681282639503479, -1.7808715105056763, 0.46008551120758057
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3301395177841187, 0.009236490353941917, 3.0219013690948486
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(611 + frame)
obj = cameras['Camera']
obj.location = 0.6715204119682312, -1.785530924797058, 0.4586084485054016
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3304082155227661, 0.005826546344906092, 3.029144048690796
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(612 + frame)
obj = cameras['Camera']
obj.location = 0.6659047603607178, -1.788706660270691, 0.4580006003379822
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3307167291641235, 0.004781192634254694, 3.0334479808807373
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(613 + frame)
obj = cameras['Camera']
obj.location = 0.6607370376586914, -1.791805386543274, 0.4566826820373535
obj.scale = 1.000000238418579, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3321723937988281, 0.0036190026439726353, 3.0361313819885254
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(614 + frame)
obj = cameras['Camera']
obj.location = 0.6546685099601746, -1.7948554754257202, 0.4558034837245941
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3335518836975098, 0.007026304490864277, 3.0378122329711914
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(615 + frame)
obj = cameras['Camera']
obj.location = 0.6484712362289429, -1.7968617677688599, 0.4557952582836151
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.3336695432662964, 0.011900939978659153, 3.039555549621582
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(616 + frame)
obj = cameras['Camera']
obj.location = 0.6418575048446655, -1.798179268836975, 0.4559606909751892
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3323496580123901, 0.01181840244680643, 3.042947292327881
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(617 + frame)
obj = cameras['Camera']
obj.location = 0.6357910633087158, -1.8002891540527344, 0.4560995399951935
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.3295814990997314, 0.007187243551015854, 3.0472958087921143
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(618 + frame)
obj = cameras['Camera']
obj.location = 0.630165696144104, -1.8037047386169434, 0.4558989703655243
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3265800476074219, 0.001854114467278123, 3.0513358116149902
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(619 + frame)
obj = cameras['Camera']
obj.location = 0.6256344318389893, -1.806486964225769, 0.4565485715866089
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.323538064956665, -0.0011239030864089727, 3.054441213607788
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(620 + frame)
obj = cameras['Camera']
obj.location = 0.6208640336990356, -1.808894395828247, 0.4571731984615326
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3207509517669678, -0.000978998839855194, 3.057614803314209
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(621 + frame)
obj = cameras['Camera']
obj.location = 0.6160516738891602, -1.811859369277954, 0.45783355832099915
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.318023443222046, 0.0007383653428405523, 3.060363531112671
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(622 + frame)
obj = cameras['Camera']
obj.location = 0.6113731861114502, -1.8147897720336914, 0.4580293893814087
obj.scale = 1.000000238418579, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3158320188522339, 0.00272606429643929, 3.0629663467407227
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(623 + frame)
obj = cameras['Camera']
obj.location = 0.6068904399871826, -1.816711664199829, 0.45840388536453247
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3136252164840698, 0.004083250183612108, 3.0659122467041016
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(624 + frame)
obj = cameras['Camera']
obj.location = 0.6042162775993347, -1.8187332153320312, 0.4586980938911438
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3110843896865845, 0.00429955031722784, 3.069636821746826
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(625 + frame)
obj = cameras['Camera']
obj.location = 0.5980675220489502, -1.8223711252212524, 0.45876502990722656
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.308328628540039, 0.004683994688093662, 3.0750374794006348
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(626 + frame)
obj = cameras['Camera']
obj.location = 0.5939739942550659, -1.8257558345794678, 0.45897188782691956
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3063842058181763, 0.005932965315878391, 3.078618049621582
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(627 + frame)
obj = cameras['Camera']
obj.location = 0.5901874303817749, -1.8286699056625366, 0.4592404067516327
obj.scale = 1.000000238418579, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3053532838821411, 0.007969317026436329, 3.081573963165283
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(628 + frame)
obj = cameras['Camera']
obj.location = 0.586149275302887, -1.8317047357559204, 0.4592900276184082
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3048357963562012, 0.01056830957531929, 3.084404945373535
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(629 + frame)
obj = cameras['Camera']
obj.location = 0.5817038416862488, -1.835366129875183, 0.45906955003738403
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3048276901245117, 0.013026939705014229, 3.0869040489196777
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(630 + frame)
obj = cameras['Camera']
obj.location = 0.5776325464248657, -1.8380862474441528, 0.4592341482639313
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.3051490783691406, 0.014391515403985977, 3.089160919189453
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(631 + frame)
obj = cameras['Camera']
obj.location = 0.5737817287445068, -1.8413772583007812, 0.4595516622066498
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.305660367012024, 0.014762715436518192, 3.0914857387542725
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(632 + frame)
obj = cameras['Camera']
obj.location = 0.5696977376937866, -1.8451474905014038, 0.4598991870880127
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3068351745605469, 0.01333246286958456, 3.0941555500030518
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(633 + frame)
obj = cameras['Camera']
obj.location = 0.565887451171875, -1.8483977317810059, 0.4595452547073364
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3090710639953613, 0.010862726718187332, 3.0967345237731934
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(634 + frame)
obj = cameras['Camera']
obj.location = 0.5619591474533081, -1.8520487546920776, 0.45930027961730957
obj.scale = 1.000000238418579, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.311469316482544, 0.008424058556556702, 3.0993287563323975
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(635 + frame)
obj = cameras['Camera']
obj.location = 0.5577497482299805, -1.8561522960662842, 0.4583997130393982
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3140511512756348, 0.004049462266266346, 3.102489709854126
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(636 + frame)
obj = cameras['Camera']
obj.location = 0.5542295575141907, -1.8596785068511963, 0.4577537178993225
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.316171646118164, -0.0011165228206664324, 3.105947971343994
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(637 + frame)
obj = cameras['Camera']
obj.location = 0.5506323575973511, -1.8639683723449707, 0.457905650138855
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.3178941011428833, -0.00405836571007967, 3.109252452850342
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(638 + frame)
obj = cameras['Camera']
obj.location = 0.5469512939453125, -1.8680810928344727, 0.4579739570617676
obj.scale = 1.000000238418579, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3202258348464966, -0.0061903297901153564, 3.1121277809143066
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(639 + frame)
obj = cameras['Camera']
obj.location = 0.5424341559410095, -1.8719942569732666, 0.45758673548698425
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3232276439666748, -0.00742920208722353, 3.114057779312134
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(640 + frame)
obj = cameras['Camera']
obj.location = 0.5378499031066895, -1.8758203983306885, 0.45673149824142456
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3261501789093018, -0.00858597457408905, 3.1150741577148438
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(641 + frame)
obj = cameras['Camera']
obj.location = 0.5336313843727112, -1.8796879053115845, 0.45580434799194336
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.327924132347107, -0.00947086326777935, 3.1158268451690674
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(642 + frame)
obj = cameras['Camera']
obj.location = 0.5301719903945923, -1.8827060461044312, 0.4561489522457123
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3281965255737305, -0.01035386323928833, 3.116727590560913
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(643 + frame)
obj = cameras['Camera']
obj.location = 0.5264312028884888, -1.8861584663391113, 0.45607790350914
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.328249216079712, -0.009894700720906258, 3.1176564693450928
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(644 + frame)
obj = cameras['Camera']
obj.location = 0.5221354961395264, -1.8891358375549316, 0.4558703303337097
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.327903151512146, -0.008418714627623558, 3.118236541748047
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(645 + frame)
obj = cameras['Camera']
obj.location = 0.518416166305542, -1.89199697971344, 0.4556446075439453
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3269087076187134, -0.005611305590718985, 3.118337631225586
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(646 + frame)
obj = cameras['Camera']
obj.location = 0.5148711204528809, -1.8953081369400024, 0.4561302363872528
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3245773315429688, -0.0029793360736221075, 3.1188464164733887
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(647 + frame)
obj = cameras['Camera']
obj.location = 0.5114154815673828, -1.8989794254302979, 0.4563145041465759
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3222161531448364, -0.0018909304635599256, 3.119689702987671
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(648 + frame)
obj = cameras['Camera']
obj.location = 0.5086315870285034, -1.9025511741638184, 0.4562116861343384
obj.scale = 1.000000238418579, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.319885015487671, -0.0009735953062772751, 3.1205265522003174
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(649 + frame)
obj = cameras['Camera']
obj.location = 0.5062897801399231, -1.905214548110962, 0.4569132924079895
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3170132637023926, 0.0011277447920292616, 3.1220383644104004
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(650 + frame)
obj = cameras['Camera']
obj.location = 0.5033842325210571, -1.9088267087936401, 0.45729172229766846
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998211860657
obj.rotation_euler = 1.3141660690307617, 0.004606220871210098, 3.12450909614563
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(651 + frame)
obj = cameras['Camera']
obj.location = 0.5000960826873779, -1.9125189781188965, 0.45740002393722534
obj.scale = 1.000000238418579, 0.9999998807907104, 0.9999998211860657
obj.rotation_euler = 1.3114956617355347, 0.008612047880887985, 3.127519130706787
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(652 + frame)
obj = cameras['Camera']
obj.location = 0.4966966509819031, -1.916377067565918, 0.4576388895511627
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3090778589248657, 0.012458693236112595, 3.1306426525115967
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(653 + frame)
obj = cameras['Camera']
obj.location = 0.49316075444221497, -1.9206562042236328, 0.4578259289264679
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.3069499731063843, 0.014935196377336979, 3.134155511856079
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(654 + frame)
obj = cameras['Camera']
obj.location = 0.48953938484191895, -1.9245994091033936, 0.45809468626976013
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.305082082748413, 0.014127831906080246, 3.138413667678833
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(655 + frame)
obj = cameras['Camera']
obj.location = 0.4863680601119995, -1.9289369583129883, 0.45784783363342285
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3038862943649292, 0.012299826368689537, -3.1403777599334717
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(656 + frame)
obj = cameras['Camera']
obj.location = 0.482505738735199, -1.9333176612854004, 0.4581243395805359
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3029165267944336, 0.009916309267282486, -3.135608434677124
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(657 + frame)
obj = cameras['Camera']
obj.location = 0.4781765341758728, -1.937438726425171, 0.4572765827178955
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999999403953552
obj.rotation_euler = 1.3028945922851562, 0.005790744908154011, -3.1305954456329346
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(658 + frame)
obj = cameras['Camera']
obj.location = 0.47458580136299133, -1.941648006439209, 0.4566968083381653
obj.scale = 1.000000238418579, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3024414777755737, 0.0020420595537871122, -3.1261255741119385
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(659 + frame)
obj = cameras['Camera']
obj.location = 0.4710302948951721, -1.9461877346038818, 0.45685020089149475
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3018114566802979, -0.0013028171379119158, -3.121579885482788
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(660 + frame)
obj = cameras['Camera']
obj.location = 0.467418909072876, -1.95063316822052, 0.45691409707069397
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3021316528320312, -0.0048200697638094425, -3.1170241832733154
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(661 + frame)
obj = cameras['Camera']
obj.location = 0.46311885118484497, -1.9547244310379028, 0.4574577212333679
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998211860657
obj.rotation_euler = 1.3028980493545532, -0.007835181429982185, -3.1127350330352783
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(662 + frame)
obj = cameras['Camera']
obj.location = 0.4587017297744751, -1.9591082334518433, 0.45627254247665405
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.304868459701538, -0.00973079726099968, -3.1092865467071533
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(663 + frame)
obj = cameras['Camera']
obj.location = 0.4548366665840149, -1.9630539417266846, 0.45594874024391174
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.306041955947876, -0.008327368646860123, -3.1064610481262207
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(664 + frame)
obj = cameras['Camera']
obj.location = 0.4497068524360657, -1.9671123027801514, 0.4563247263431549
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.3064213991165161, -0.006946354638785124, -3.1023993492126465
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(665 + frame)
obj = cameras['Camera']
obj.location = 0.44425803422927856, -1.9709850549697876, 0.45660480856895447
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998211860657
obj.rotation_euler = 1.3065470457077026, -0.008974434807896614, -3.0979344844818115
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(666 + frame)
obj = cameras['Camera']
obj.location = 0.4400278925895691, -1.9748811721801758, 0.45656487345695496
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3063123226165771, -0.01392304990440607, -3.094245433807373
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(667 + frame)
obj = cameras['Camera']
obj.location = 0.4368288516998291, -1.978532314300537, 0.4559866487979889
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.3061468601226807, -0.01791648380458355, -3.091078042984009
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(668 + frame)
obj = cameras['Camera']
obj.location = 0.4342386722564697, -1.982414722442627, 0.455130934715271
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.3054794073104858, -0.01773998700082302, -3.087930679321289
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(669 + frame)
obj = cameras['Camera']
obj.location = 0.4292789101600647, -1.986358880996704, 0.45480045676231384
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998211860657
obj.rotation_euler = 1.3037580251693726, -0.012674890458583832, -3.083355188369751
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(670 + frame)
obj = cameras['Camera']
obj.location = 0.4257770776748657, -1.99051833152771, 0.45538589358329773
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.3013843297958374, -0.007356303744018078, -3.081533670425415
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(671 + frame)
obj = cameras['Camera']
obj.location = 0.4223986864089966, -1.994189739227295, 0.45550328493118286
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2993718385696411, -0.005603125784546137, -3.079483985900879
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(672 + frame)
obj = cameras['Camera']
obj.location = 0.4211636781692505, -1.998176097869873, 0.4550839066505432
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2970625162124634, -0.00594269810244441, -3.0767922401428223
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(673 + frame)
obj = cameras['Camera']
obj.location = 0.4214499592781067, -2.0019266605377197, 0.45505768060684204
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2940726280212402, -0.0044914777390658855, -3.068660020828247
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(674 + frame)
obj = cameras['Camera']
obj.location = 0.41396671533584595, -2.006929636001587, 0.4545941650867462
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2911349534988403, -0.0028364984318614006, -3.0595288276672363
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(675 + frame)
obj = cameras['Camera']
obj.location = 0.4112483263015747, -2.0111935138702393, 0.454547643661499
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2883340120315552, -0.0021225681994110346, -3.055797815322876
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(676 + frame)
obj = cameras['Camera']
obj.location = 0.40900254249572754, -2.0150306224823, 0.4541952610015869
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2860745191574097, -0.0008387932903133333, -3.053187847137451
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(677 + frame)
obj = cameras['Camera']
obj.location = 0.4065941572189331, -2.0194196701049805, 0.45355311036109924
obj.scale = 1.000000238418579, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.283929705619812, 0.0012176306918263435, -3.051159143447876
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(678 + frame)
obj = cameras['Camera']
obj.location = 0.40427887439727783, -2.0239040851593018, 0.4534580111503601
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2815817594528198, 0.0029425034299492836, -3.04915189743042
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(679 + frame)
obj = cameras['Camera']
obj.location = 0.4022192358970642, -2.0285258293151855, 0.45387500524520874
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2791155576705933, 0.002384437248110771, -3.0464534759521484
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(680 + frame)
obj = cameras['Camera']
obj.location = 0.400071918964386, -2.0327539443969727, 0.4542616307735443
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.277387261390686, -0.00019653762865345925, -3.043671131134033
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(681 + frame)
obj = cameras['Camera']
obj.location = 0.39828312397003174, -2.036949872970581, 0.45416635274887085
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2765047550201416, -0.003958278801292181, -3.0418596267700195
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(682 + frame)
obj = cameras['Camera']
obj.location = 0.39718949794769287, -2.0412302017211914, 0.45387279987335205
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2763588428497314, -0.007348058745265007, -3.0415637493133545
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(683 + frame)
obj = cameras['Camera']
obj.location = 0.3958936929702759, -2.0461504459381104, 0.45360875129699707
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.276455283164978, -0.009615196846425533, -3.04177188873291
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(684 + frame)
obj = cameras['Camera']
obj.location = 0.3943840265274048, -2.051525592803955, 0.452900767326355
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2770538330078125, -0.011380556970834732, -3.0421721935272217
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(685 + frame)
obj = cameras['Camera']
obj.location = 0.39289987087249756, -2.0571579933166504, 0.45261913537979126
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2772037982940674, -0.013707756996154785, -3.042746067047119
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(686 + frame)
obj = cameras['Camera']
obj.location = 0.3917251229286194, -2.0631022453308105, 0.45270320773124695
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2776869535446167, -0.016833161935210228, -3.0432868003845215
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(687 + frame)
obj = cameras['Camera']
obj.location = 0.39075446128845215, -2.069152593612671, 0.45260828733444214
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2785879373550415, -0.021258216351270676, -3.0430994033813477
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(688 + frame)
obj = cameras['Camera']
obj.location = 0.39053404331207275, -2.074991464614868, 0.45151907205581665
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2799214124679565, -0.02427966147661209, -3.0422027111053467
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(689 + frame)
obj = cameras['Camera']
obj.location = 0.38909637928009033, -2.080533027648926, 0.4505729675292969
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.280638575553894, -0.0236609298735857, -3.040769100189209
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(690 + frame)
obj = cameras['Camera']
obj.location = 0.3872882127761841, -2.0854439735412598, 0.44907665252685547
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2811540365219116, -0.022922225296497345, -3.0392351150512695
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(691 + frame)
obj = cameras['Camera']
obj.location = 0.38530927896499634, -2.0903117656707764, 0.4485200047492981
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.280569314956665, -0.022859672084450722, -3.03741717338562
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(692 + frame)
obj = cameras['Camera']
obj.location = 0.3836100101470947, -2.0945980548858643, 0.4474673569202423
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.279585361480713, -0.02354493923485279, -3.0355708599090576
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(693 + frame)
obj = cameras['Camera']
obj.location = 0.38224512338638306, -2.098792314529419, 0.44613996148109436
obj.scale = 1.000000238418579, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2775977849960327, -0.02183685079216957, -3.0341403484344482
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(694 + frame)
obj = cameras['Camera']
obj.location = 0.3809084892272949, -2.103454351425171, 0.44601500034332275
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2739862203598022, -0.017730051651597023, -3.032552480697632
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(695 + frame)
obj = cameras['Camera']
obj.location = 0.3799366354942322, -2.1081743240356445, 0.44658705592155457
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2697559595108032, -0.015302957035601139, -3.030661106109619
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(696 + frame)
obj = cameras['Camera']
obj.location = 0.3796091675758362, -2.1125338077545166, 0.44658923149108887
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2662655115127563, -0.014719746075570583, -3.0286519527435303
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(697 + frame)
obj = cameras['Camera']
obj.location = 0.37977534532546997, -2.1170945167541504, 0.4468529522418976
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2627849578857422, -0.013202051632106304, -3.0266048908233643
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(698 + frame)
obj = cameras['Camera']
obj.location = 0.37907958030700684, -2.1214354038238525, 0.44689425826072693
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2599425315856934, -0.01055410411208868, -3.0243771076202393
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(699 + frame)
obj = cameras['Camera']
obj.location = 0.37785792350769043, -2.1253390312194824, 0.44653815031051636
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.257783055305481, -0.007714805658906698, -3.0226333141326904
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(700 + frame)
obj = cameras['Camera']
obj.location = 0.3766375780105591, -2.1291003227233887, 0.4463212490081787
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2553343772888184, -0.00603816332295537, -3.021362066268921
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(701 + frame)
obj = cameras['Camera']
obj.location = 0.3758540749549866, -2.133037805557251, 0.44568854570388794
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2528526782989502, -0.005446041934192181, -3.0202476978302
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(702 + frame)
obj = cameras['Camera']
obj.location = 0.3751310110092163, -2.1374521255493164, 0.44518962502479553
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998211860657
obj.rotation_euler = 1.2500922679901123, -0.0049455431289970875, -3.019333600997925
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(703 + frame)
obj = cameras['Camera']
obj.location = 0.3748801350593567, -2.142033100128174, 0.4446013569831848
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2474864721298218, -0.00444646505638957, -3.0186917781829834
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(704 + frame)
obj = cameras['Camera']
obj.location = 0.37484073638916016, -2.1468944549560547, 0.4441695213317871
obj.scale = 1.000000238418579, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2449545860290527, -0.0029499174561351538, -3.017845869064331
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(705 + frame)
obj = cameras['Camera']
obj.location = 0.37454938888549805, -2.152036190032959, 0.443888783454895
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2425426244735718, -0.0008791199652478099, -3.016383171081543
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(706 + frame)
obj = cameras['Camera']
obj.location = 0.3749121427536011, -2.157073736190796, 0.4434860050678253
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2406673431396484, 0.0014678656589239836, -3.014899969100952
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(707 + frame)
obj = cameras['Camera']
obj.location = 0.37450504302978516, -2.1624627113342285, 0.443170428276062
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998211860657
obj.rotation_euler = 1.2393276691436768, 0.00418655201792717, -3.0126826763153076
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(708 + frame)
obj = cameras['Camera']
obj.location = 0.37337708473205566, -2.167670249938965, 0.44287118315696716
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2386913299560547, 0.004843713715672493, -3.0097224712371826
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(709 + frame)
obj = cameras['Camera']
obj.location = 0.3728446960449219, -2.172820568084717, 0.4422076940536499
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2387089729309082, 0.004350029863417149, -3.006983518600464
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(710 + frame)
obj = cameras['Camera']
obj.location = 0.3722308278083801, -2.1781067848205566, 0.4420963227748871
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2390975952148438, 0.004417861811816692, -3.0045182704925537
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(711 + frame)
obj = cameras['Camera']
obj.location = 0.37159842252731323, -2.1835110187530518, 0.4423697590827942
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2402358055114746, 0.002721066353842616, -3.001936674118042
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(712 + frame)
obj = cameras['Camera']
obj.location = 0.3720310926437378, -2.191251277923584, 0.44743579626083374
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2405669689178467, -0.0002788211568258703, -2.9995381832122803
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(713 + frame)
obj = cameras['Camera']
obj.location = 0.36890697479248047, -2.196044445037842, 0.441945344209671
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.253387212753296, -0.0006095729186199605, -2.9968693256378174
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(714 + frame)
obj = cameras['Camera']
obj.location = 0.36733245849609375, -2.200684070587158, 0.43906059861183167
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2584110498428345, -0.0018029301427304745, -2.995516061782837
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(715 + frame)
obj = cameras['Camera']
obj.location = 0.36742156744003296, -2.2048158645629883, 0.4374464452266693
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2608426809310913, 0.0008747280808165669, -2.9956753253936768
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(716 + frame)
obj = cameras['Camera']
obj.location = 0.36722826957702637, -2.2091126441955566, 0.4381880462169647
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2601137161254883, 0.007291292306035757, -2.9956517219543457
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(717 + frame)
obj = cameras['Camera']
obj.location = 0.3661797046661377, -2.2145488262176514, 0.4382583498954773
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.258754014968872, 0.01225260365754366, -2.993776321411133
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(718 + frame)
obj = cameras['Camera']
obj.location = 0.3652139902114868, -2.220022678375244, 0.43825894594192505
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998211860657
obj.rotation_euler = 1.2575182914733887, 0.016253046691417694, -2.99161434173584
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(719 + frame)
obj = cameras['Camera']
obj.location = 0.3636370897293091, -2.22466778755188, 0.4384751319885254
obj.scale = 1.000000238418579, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2564266920089722, 0.018152540549635887, -2.988548994064331
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(720 + frame)
obj = cameras['Camera']
obj.location = 0.3621687889099121, -2.228802442550659, 0.4378252625465393
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.255308985710144, 0.018016107380390167, -2.9850528240203857
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(721 + frame)
obj = cameras['Camera']
obj.location = 0.3612115979194641, -2.233290195465088, 0.4375712275505066
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2534440755844116, 0.01901097595691681, -2.982048749923706
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(722 + frame)
obj = cameras['Camera']
obj.location = 0.360731303691864, -2.239372730255127, 0.4358048439025879
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2518351078033447, 0.018869122490286827, -2.978914976119995
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(723 + frame)
obj = cameras['Camera']
obj.location = 0.3600262999534607, -2.243377208709717, 0.4354337751865387
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2491637468338013, 0.018548689782619476, -2.9759318828582764
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(724 + frame)
obj = cameras['Camera']
obj.location = 0.3597211241722107, -2.24912428855896, 0.434055894613266
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998211860657
obj.rotation_euler = 1.2464807033538818, 0.02004619874060154, -2.9738080501556396
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(725 + frame)
obj = cameras['Camera']
obj.location = 0.35938286781311035, -2.254122257232666, 0.43306848406791687
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2439755201339722, 0.021380608901381493, -2.9723122119903564
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(726 + frame)
obj = cameras['Camera']
obj.location = 0.3594266176223755, -2.2578482627868652, 0.43290695548057556
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2413909435272217, 0.021275583654642105, -2.971238374710083
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(727 + frame)
obj = cameras['Camera']
obj.location = 0.3593398332595825, -2.261932611465454, 0.4323665499687195
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998211860657
obj.rotation_euler = 1.239046573638916, 0.02032938227057457, -2.9699740409851074
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(728 + frame)
obj = cameras['Camera']
obj.location = 0.3595106601715088, -2.2659614086151123, 0.4316078722476959
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2370243072509766, 0.018310165032744408, -2.968850612640381
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(729 + frame)
obj = cameras['Camera']
obj.location = 0.3601447343826294, -2.269624710083008, 0.4306168556213379
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.235554575920105, 0.017125943675637245, -2.968791961669922
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(730 + frame)
obj = cameras['Camera']
obj.location = 0.36068958044052124, -2.27327561378479, 0.42993634939193726
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2339733839035034, 0.01650187186896801, -2.969128131866455
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(731 + frame)
obj = cameras['Camera']
obj.location = 0.36126554012298584, -2.276756525039673, 0.4293312430381775
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2324929237365723, 0.013939665630459785, -2.9692070484161377
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(732 + frame)
obj = cameras['Camera']
obj.location = 0.3623628616333008, -2.279629945755005, 0.4288714528083801
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2310370206832886, 0.009408870711922646, -2.9693667888641357
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(733 + frame)
obj = cameras['Camera']
obj.location = 0.36413031816482544, -2.2818167209625244, 0.4290229082107544
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2292683124542236, 0.006525449920445681, -2.970259189605713
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(734 + frame)
obj = cameras['Camera']
obj.location = 0.3658747673034668, -2.284785747528076, 0.4283864498138428
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2277177572250366, 0.007239704951643944, -2.971903085708618
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(735 + frame)
obj = cameras['Camera']
obj.location = 0.36724019050598145, -2.288179874420166, 0.4276166558265686
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2260538339614868, 0.007375806570053101, -2.9734506607055664
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(736 + frame)
obj = cameras['Camera']
obj.location = 0.3692282438278198, -2.2910404205322266, 0.4275170862674713
obj.scale = 1.000000238418579, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2244811058044434, 0.004385149106383324, -2.9748647212982178
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(737 + frame)
obj = cameras['Camera']
obj.location = 0.37196147441864014, -2.293379068374634, 0.4279778003692627
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2231311798095703, 0.000629187619779259, -2.9764020442962646
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(738 + frame)
obj = cameras['Camera']
obj.location = 0.37503087520599365, -2.2956416606903076, 0.42854368686676025
obj.scale = 1.000000238418579, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2225239276885986, -0.0020232012029737234, -2.9782493114471436
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(739 + frame)
obj = cameras['Camera']
obj.location = 0.3776865005493164, -2.298147201538086, 0.428195059299469
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2231128215789795, -0.003169051371514797, -2.9803571701049805
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(740 + frame)
obj = cameras['Camera']
obj.location = 0.37861931324005127, -2.3012194633483887, 0.4279346764087677
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2239205837249756, -0.003395611420273781, -2.982434034347534
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(741 + frame)
obj = cameras['Camera']
obj.location = 0.3827638030052185, -2.3032236099243164, 0.4279864430427551
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2255498170852661, -0.0022886174265295267, -2.989579916000366
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(742 + frame)
obj = cameras['Camera']
obj.location = 0.38495856523513794, -2.304861068725586, 0.4272589683532715
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2275068759918213, -9.728862642077729e-05, -2.99479341506958
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(743 + frame)
obj = cameras['Camera']
obj.location = 0.388439416885376, -2.307257890701294, 0.4265790283679962
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2279727458953857, 0.0009446688345633447, -3.00050950050354
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(744 + frame)
obj = cameras['Camera']
obj.location = 0.3914337158203125, -2.3091306686401367, 0.42666321992874146
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2278285026550293, 0.001235084724612534, -3.005424976348877
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(745 + frame)
obj = cameras['Camera']
obj.location = 0.39539986848831177, -2.31026291847229, 0.42623403668403625
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2277991771697998, -0.001218977733515203, -3.01001238822937
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(746 + frame)
obj = cameras['Camera']
obj.location = 0.4000306725502014, -2.3118081092834473, 0.42639052867889404
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.226218581199646, -0.0022285678423941135, -3.014199733734131
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(747 + frame)
obj = cameras['Camera']
obj.location = 0.4049195647239685, -2.313159942626953, 0.4279465079307556
obj.scale = 1.000000238418579, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2237423658370972, 0.00038904359098523855, -3.0186285972595215
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(748 + frame)
obj = cameras['Camera']
obj.location = 0.4092295169830322, -2.314582109451294, 0.4294317960739136
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2219188213348389, 0.005284114275127649, -3.0227253437042236
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(749 + frame)
obj = cameras['Camera']
obj.location = 0.4121653437614441, -2.317007064819336, 0.4292519688606262
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2211711406707764, 0.0084169777110219, -3.0252525806427
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(750 + frame)
obj = cameras['Camera']
obj.location = 0.4155312180519104, -2.319094657897949, 0.42973822355270386
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2203726768493652, 0.007120175752788782, -3.0268101692199707
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(751 + frame)
obj = cameras['Camera']
obj.location = 0.4191185235977173, -2.3209388256073, 0.4298202395439148
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2205287218093872, 0.0039058804977685213, -3.027778387069702
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(752 + frame)
obj = cameras['Camera']
obj.location = 0.4206050634384155, -2.322314977645874, 0.43005189299583435
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2210540771484375, -0.00012077331484761089, -3.0273849964141846
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(753 + frame)
obj = cameras['Camera']
obj.location = 0.4241611957550049, -2.3238039016723633, 0.42999470233917236
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2224857807159424, -0.005276257637888193, -3.028071165084839
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(754 + frame)
obj = cameras['Camera']
obj.location = 0.42830657958984375, -2.3246958255767822, 0.4305912256240845
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.223321557044983, -0.009642291814088821, -3.0290367603302
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(755 + frame)
obj = cameras['Camera']
obj.location = 0.4324082136154175, -2.32570219039917, 0.431122362613678
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2232834100723267, -0.011755367740988731, -3.0302023887634277
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(756 + frame)
obj = cameras['Camera']
obj.location = 0.43622887134552, -2.326521396636963, 0.4313428997993469
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2231727838516235, -0.010897628962993622, -3.031282663345337
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(757 + frame)
obj = cameras['Camera']
obj.location = 0.4402928352355957, -2.3280091285705566, 0.43164902925491333
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2228869199752808, -0.009494220837950706, -3.032209634780884
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(758 + frame)
obj = cameras['Camera']
obj.location = 0.4438730478286743, -2.329404830932617, 0.43111300468444824
obj.scale = 1.000000238418579, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2231876850128174, -0.0100258132442832, -3.0319278240203857
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(759 + frame)
obj = cameras['Camera']
obj.location = 0.4466531276702881, -2.3319785594940186, 0.43052759766578674
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2231141328811646, -0.011598682962357998, -3.030808925628662
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(760 + frame)
obj = cameras['Camera']
obj.location = 0.4495028257369995, -2.3347327709198, 0.43016868829727173
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.222995400428772, -0.011896411888301373, -3.03054141998291
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(761 + frame)
obj = cameras['Camera']
obj.location = 0.45262449979782104, -2.3369827270507812, 0.4294227659702301
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2231873273849487, -0.011920271441340446, -3.0310165882110596
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(762 + frame)
obj = cameras['Camera']
obj.location = 0.4555036425590515, -2.339272975921631, 0.42866769433021545
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2231541872024536, -0.011437715962529182, -3.0316455364227295
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(763 + frame)
obj = cameras['Camera']
obj.location = 0.45829296112060547, -2.342388153076172, 0.4279816448688507
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2227307558059692, -0.00961287785321474, -3.033367872238159
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(764 + frame)
obj = cameras['Camera']
obj.location = 0.4614485502243042, -2.3453168869018555, 0.42726320028305054
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2225531339645386, -0.0064235953614115715, -3.0361530780792236
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(765 + frame)
obj = cameras['Camera']
obj.location = 0.464816689491272, -2.3476057052612305, 0.42645063996315
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2231738567352295, -0.002363508800044656, -3.0396628379821777
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(766 + frame)
obj = cameras['Camera']
obj.location = 0.46759286522865295, -2.3509979248046875, 0.424609512090683
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2240047454833984, -0.0006082078907638788, -3.0425913333892822
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(767 + frame)
obj = cameras['Camera']
obj.location = 0.46314942836761475, -2.355475664138794, 0.42423564195632935
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2242963314056396, -0.0018927092896774411, -3.0460901260375977
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(768 + frame)
obj = cameras['Camera']
obj.location = 0.4737589955329895, -2.356980323791504, 0.42467451095581055
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2256345748901367, -0.004695038311183453, -3.05877423286438
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(769 + frame)
obj = cameras['Camera']
obj.location = 0.4798278510570526, -2.359189510345459, 0.4249129593372345
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2279335260391235, -0.0017441193340346217, -3.0646376609802246
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(770 + frame)
obj = cameras['Camera']
obj.location = 0.48444777727127075, -2.361680269241333, 0.42513275146484375
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2293462753295898, 0.0018212954746559262, -3.069207191467285
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(771 + frame)
obj = cameras['Camera']
obj.location = 0.4887981116771698, -2.3640241622924805, 0.42492640018463135
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2302545309066772, 0.0027934645768254995, -3.0725443363189697
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(772 + frame)
obj = cameras['Camera']
obj.location = 0.4931914508342743, -2.3664071559906006, 0.4252464771270752
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.230478286743164, 4.644039654522203e-05, -3.0744762420654297
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(773 + frame)
obj = cameras['Camera']
obj.location = 0.4990686774253845, -2.36735200881958, 0.42720210552215576
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.229846715927124, -0.0027456204406917095, -3.076725959777832
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(774 + frame)
obj = cameras['Camera']
obj.location = 0.5044015645980835, -2.3683695793151855, 0.42821672558784485
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2297322750091553, -0.0010400019818916917, -3.078538179397583
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(775 + frame)
obj = cameras['Camera']
obj.location = 0.5084296464920044, -2.370488405227661, 0.42835575342178345
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2300658226013184, 0.0022867931984364986, -3.0793356895446777
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(776 + frame)
obj = cameras['Camera']
obj.location = 0.5115717053413391, -2.373823404312134, 0.427925169467926
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2307335138320923, 0.0033809172455221415, -3.0794308185577393
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(777 + frame)
obj = cameras['Camera']
obj.location = 0.5149311423301697, -2.376332998275757, 0.4274039566516876
obj.scale = 1.000000238418579, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.231484293937683, 0.0016258393879979849, -3.0795273780822754
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(778 + frame)
obj = cameras['Camera']
obj.location = 0.51873379945755, -2.3788163661956787, 0.4268589913845062
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2322533130645752, -0.0029695904813706875, -3.0796451568603516
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(779 + frame)
obj = cameras['Camera']
obj.location = 0.5230391621589661, -2.3814175128936768, 0.4269621670246124
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.232384204864502, -0.008563784882426262, -3.079901933670044
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(780 + frame)
obj = cameras['Camera']
obj.location = 0.5272656679153442, -2.3832144737243652, 0.4271157383918762
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2329260110855103, -0.01116155181080103, -3.0812854766845703
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(781 + frame)
obj = cameras['Camera']
obj.location = 0.5315796732902527, -2.3858556747436523, 0.4266599416732788
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2335782051086426, -0.011885144747793674, -3.0835988521575928
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(782 + frame)
obj = cameras['Camera']
obj.location = 0.5347853302955627, -2.387944221496582, 0.4265103340148926
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2341570854187012, -0.01108191255480051, -3.0872292518615723
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(783 + frame)
obj = cameras['Camera']
obj.location = 0.5376267433166504, -2.3899166584014893, 0.4259289503097534
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998211860657
obj.rotation_euler = 1.2351596355438232, -0.010454178787767887, -3.092071533203125
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(784 + frame)
obj = cameras['Camera']
obj.location = 0.5388569831848145, -2.392209768295288, 0.4256978929042816
obj.scale = 1.000000238418579, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.235799789428711, -0.011225352063775063, -3.0992910861968994
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(785 + frame)
obj = cameras['Camera']
obj.location = 0.5466061234474182, -2.393718957901001, 0.4256499409675598
obj.scale = 1.000000238418579, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2362072467803955, -0.013185824267566204, -3.112729787826538
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(786 + frame)
obj = cameras['Camera']
obj.location = 0.5550480484962463, -2.3953638076782227, 0.4257310926914215
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.23660409450531, -0.01139212679117918, -3.1241884231567383
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(787 + frame)
obj = cameras['Camera']
obj.location = 0.5607777833938599, -2.3968026638031006, 0.4248911142349243
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2375108003616333, -0.006639132741838694, -3.133565664291382
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(788 + frame)
obj = cameras['Camera']
obj.location = 0.5689479112625122, -2.397953987121582, 0.42524254322052
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2374924421310425, -0.0030199335888028145, 3.1383309364318848
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(789 + frame)
obj = cameras['Camera']
obj.location = 0.5751998424530029, -2.399320602416992, 0.4246155619621277
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2379316091537476, 0.0037044009659439325, 3.1300711631774902
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(790 + frame)
obj = cameras['Camera']
obj.location = 0.5778760313987732, -2.4006426334381104, 0.42175209522247314
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998211860657
obj.rotation_euler = 1.2394872903823853, 0.006823670119047165, 3.1219418048858643
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(791 + frame)
obj = cameras['Camera']
obj.location = 0.5844395160675049, -2.402437686920166, 0.4224713444709778
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2382956743240356, 0.006529650650918484, 3.1096761226654053
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(792 + frame)
obj = cameras['Camera']
obj.location = 0.5926026701927185, -2.403156042098999, 0.4226263761520386
obj.scale = 1.000000238418579, 0.9999998807907104, 0.9999998211860657
obj.rotation_euler = 1.237135410308838, 0.004663720261305571, 3.098771333694458
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(793 + frame)
obj = cameras['Camera']
obj.location = 0.5985574722290039, -2.4047389030456543, 0.42270490527153015
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2354398965835571, 0.0028162901289761066, 3.0888259410858154
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(794 + frame)
obj = cameras['Camera']
obj.location = 0.6099144220352173, -2.4063401222229004, 0.4236803650856018
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.233238697052002, 0.0007740213768556714, 3.077617883682251
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(795 + frame)
obj = cameras['Camera']
obj.location = 0.6162258386611938, -2.407662868499756, 0.42460718750953674
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2317732572555542, 0.004696656949818134, 3.072052240371704
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(796 + frame)
obj = cameras['Camera']
obj.location = 0.6222103238105774, -2.4086761474609375, 0.4246065318584442
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2317241430282593, 0.007851122878491879, 3.066389560699463
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(797 + frame)
obj = cameras['Camera']
obj.location = 0.6287800073623657, -2.410191297531128, 0.42452216148376465
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2319847345352173, 0.006395232398062944, 3.06135892868042
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(798 + frame)
obj = cameras['Camera']
obj.location = 0.6342753171920776, -2.411003828048706, 0.42455601692199707
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2317458391189575, 0.0008159229764714837, 3.058627128601074
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(799 + frame)
obj = cameras['Camera']
obj.location = 0.6344687938690186, -2.412895679473877, 0.42450079321861267
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2306065559387207, -0.0071973600424826145, 3.055842638015747
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(800 + frame)
obj = cameras['Camera']
obj.location = 0.6394509077072144, -2.4150805473327637, 0.42438530921936035
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2295583486557007, -0.015395733527839184, 3.045646905899048
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(801 + frame)
obj = cameras['Camera']
obj.location = 0.6490010619163513, -2.417440891265869, 0.4238070845603943
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2296375036239624, -0.020284980535507202, 3.0349671840667725
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(802 + frame)
obj = cameras['Camera']
obj.location = 0.656891942024231, -2.420030117034912, 0.4233962297439575
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2299975156784058, -0.020830495283007622, 3.0289676189422607
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(803 + frame)
obj = cameras['Camera']
obj.location = 0.6620036959648132, -2.4223031997680664, 0.42300036549568176
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998211860657
obj.rotation_euler = 1.230257272720337, -0.02127688378095627, 3.025887966156006
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(804 + frame)
obj = cameras['Camera']
obj.location = 0.6674791574478149, -2.4241626262664795, 0.4223894476890564
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998211860657
obj.rotation_euler = 1.2308788299560547, -0.022996440529823303, 3.0232110023498535
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(805 + frame)
obj = cameras['Camera']
obj.location = 0.6724971532821655, -2.4266557693481445, 0.4210302531719208
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2315189838409424, -0.027423443272709846, 3.0218098163604736
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(806 + frame)
obj = cameras['Camera']
obj.location = 0.6766902208328247, -2.429495334625244, 0.4202244281768799
obj.scale = 1.000000238418579, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2312052249908447, -0.03208353742957115, 3.020752429962158
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(807 + frame)
obj = cameras['Camera']
obj.location = 0.680443525314331, -2.4322376251220703, 0.4198293685913086
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.230784296989441, -0.03492103889584541, 3.018406867980957
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(808 + frame)
obj = cameras['Camera']
obj.location = 0.6850013732910156, -2.4347243309020996, 0.419415146112442
obj.scale = 1.000000238418579, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2309319972991943, -0.03588275983929634, 3.0156078338623047
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(809 + frame)
obj = cameras['Camera']
obj.location = 0.6892015933990479, -2.437528610229492, 0.4186113774776459
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2314989566802979, -0.034566860646009445, 3.012441635131836
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(810 + frame)
obj = cameras['Camera']
obj.location = 0.693527340888977, -2.4408693313598633, 0.4174764156341553
obj.scale = 1.000000238418579, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2322221994400024, -0.032847944647073746, 3.0087242126464844
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(811 + frame)
obj = cameras['Camera']
obj.location = 0.6931262016296387, -2.442715644836426, 0.4171026051044464
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2327688932418823, -0.03225599601864815, 3.0054824352264404
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(812 + frame)
obj = cameras['Camera']
obj.location = 0.697263777256012, -2.446241617202759, 0.4165467321872711
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2341117858886719, -0.03214198723435402, 2.995366334915161
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(813 + frame)
obj = cameras['Camera']
obj.location = 0.7055512070655823, -2.4506821632385254, 0.41597896814346313
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2357707023620605, -0.029695533215999603, 2.9852304458618164
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(814 + frame)
obj = cameras['Camera']
obj.location = 0.7108768820762634, -2.454606533050537, 0.4158990681171417
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2371087074279785, -0.025056513026356697, 2.9795844554901123
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(815 + frame)
obj = cameras['Camera']
obj.location = 0.7134461402893066, -2.45821475982666, 0.4159296154975891
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998211860657
obj.rotation_euler = 1.237139105796814, -0.02319643460214138, 2.973907470703125
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(816 + frame)
obj = cameras['Camera']
obj.location = 0.720365047454834, -2.4618465900421143, 0.4162263572216034
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2359189987182617, -0.023236684501171112, 2.9664533138275146
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(817 + frame)
obj = cameras['Camera']
obj.location = 0.725337564945221, -2.464731216430664, 0.41610878705978394
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.234979510307312, -0.023594506084918976, 2.9620091915130615
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(818 + frame)
obj = cameras['Camera']
obj.location = 0.7315473556518555, -2.4682319164276123, 0.41636356711387634
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2339451313018799, -0.027428625151515007, 2.958094596862793
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(819 + frame)
obj = cameras['Camera']
obj.location = 0.7362436056137085, -2.470615863800049, 0.4165138006210327
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.233352541923523, -0.030397756025195122, 2.9561846256256104
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(820 + frame)
obj = cameras['Camera']
obj.location = 0.7407287359237671, -2.473024845123291, 0.41645383834838867
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2327009439468384, -0.03288966044783592, 2.954277515411377
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(821 + frame)
obj = cameras['Camera']
obj.location = 0.7451400756835938, -2.4756579399108887, 0.4163995683193207
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2313501834869385, -0.03762589022517204, 2.952598810195923
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(822 + frame)
obj = cameras['Camera']
obj.location = 0.7460549473762512, -2.477738380432129, 0.4166065752506256
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2295390367507935, -0.04356212541460991, 2.9496779441833496
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(823 + frame)
obj = cameras['Camera']
obj.location = 0.7515017986297607, -2.4806411266326904, 0.41664400696754456
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2280735969543457, -0.04720520228147507, 2.94283390045166
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(824 + frame)
obj = cameras['Camera']
obj.location = 0.7557435035705566, -2.483428955078125, 0.4162813425064087
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2271968126296997, -0.050094470381736755, 2.9368221759796143
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(825 + frame)
obj = cameras['Camera']
obj.location = 0.7606462240219116, -2.486161470413208, 0.4157884120941162
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2262706756591797, -0.05390935391187668, 2.930148124694824
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(826 + frame)
obj = cameras['Camera']
obj.location = 0.765037477016449, -2.4887027740478516, 0.4158061146736145
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2247225046157837, -0.05714982748031616, 2.924081563949585
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(827 + frame)
obj = cameras['Camera']
obj.location = 0.7690186500549316, -2.4916818141937256, 0.4155833423137665
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2235733270645142, -0.0577373281121254, 2.9163644313812256
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(828 + frame)
obj = cameras['Camera']
obj.location = 0.7728762626647949, -2.4941720962524414, 0.4152863621711731
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2231999635696411, -0.057668063789606094, 2.9079530239105225
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(829 + frame)
obj = cameras['Camera']
obj.location = 0.7764729261398315, -2.4969544410705566, 0.41513264179229736
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998211860657
obj.rotation_euler = 1.2233730554580688, -0.058790795505046844, 2.897216796875
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(830 + frame)
obj = cameras['Camera']
obj.location = 0.7830309867858887, -2.4999032020568848, 0.4147191047668457
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2241562604904175, -0.05980735644698143, 2.8849360942840576
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(831 + frame)
obj = cameras['Camera']
obj.location = 0.7883490324020386, -2.5031256675720215, 0.41413190960884094
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.224960446357727, -0.060349736362695694, 2.8733980655670166
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(832 + frame)
obj = cameras['Camera']
obj.location = 0.7966994047164917, -2.506958484649658, 0.41364383697509766
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2257729768753052, -0.05969848856329918, 2.860856533050537
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(833 + frame)
obj = cameras['Camera']
obj.location = 0.8026306629180908, -2.5117368698120117, 0.41184112429618835
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2270060777664185, -0.056925710290670395, 2.8519232273101807
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(834 + frame)
obj = cameras['Camera']
obj.location = 0.8053456544876099, -2.513746976852417, 0.411776065826416
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998211860657
obj.rotation_euler = 1.2275453805923462, -0.05377921089529991, 2.843543529510498
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(835 + frame)
obj = cameras['Camera']
obj.location = 0.8117258548736572, -2.5177698135375977, 0.41104915738105774
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2281173467636108, -0.055180393159389496, 2.8294477462768555
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(836 + frame)
obj = cameras['Camera']
obj.location = 0.8219280242919922, -2.521149158477783, 0.4110511541366577
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2279384136199951, -0.05606870725750923, 2.8166959285736084
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(837 + frame)
obj = cameras['Camera']
obj.location = 0.8288692235946655, -2.5239639282226562, 0.41083043813705444
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2275785207748413, -0.05634389817714691, 2.8068346977233887
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(838 + frame)
obj = cameras['Camera']
obj.location = 0.837945818901062, -2.5274288654327393, 0.4113273620605469
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2263140678405762, -0.05645817518234253, 2.7966604232788086
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(839 + frame)
obj = cameras['Camera']
obj.location = 0.8471542596817017, -2.531052350997925, 0.41232919692993164
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2247169017791748, -0.05256988853216171, 2.788891315460205
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(840 + frame)
obj = cameras['Camera']
obj.location = 0.8535361289978027, -2.5324981212615967, 0.412929505109787
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2242275476455688, -0.047112125903367996, 2.7845609188079834
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(841 + frame)
obj = cameras['Camera']
obj.location = 0.8600019216537476, -2.5342345237731934, 0.4131811857223511
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2241564989089966, -0.04294963553547859, 2.7807559967041016
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(842 + frame)
obj = cameras['Camera']
obj.location = 0.8646522760391235, -2.5373823642730713, 0.41197434067726135
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2249172925949097, -0.038331061601638794, 2.7773032188415527
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(843 + frame)
obj = cameras['Camera']
obj.location = 0.8644158840179443, -2.5377628803253174, 0.41250279545783997
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.225198745727539, -0.038277726620435715, 2.772327184677124
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(844 + frame)
obj = cameras['Camera']
obj.location = 0.8710532784461975, -2.5406746864318848, 0.4121127724647522
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998211860657
obj.rotation_euler = 1.2265325784683228, -0.04081258922815323, 2.761162519454956
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(845 + frame)
obj = cameras['Camera']
obj.location = 0.8792463541030884, -2.5433552265167236, 0.41226857900619507
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2273658514022827, -0.043645091354846954, 2.752521276473999
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(846 + frame)
obj = cameras['Camera']
obj.location = 0.8842542767524719, -2.5459439754486084, 0.412602961063385
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2274311780929565, -0.044415563344955444, 2.7474863529205322
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(847 + frame)
obj = cameras['Camera']
obj.location = 0.8876919746398926, -2.548348903656006, 0.41185158491134644
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.227912187576294, -0.04524360969662666, 2.7422239780426025
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(848 + frame)
obj = cameras['Camera']
obj.location = 0.8942061066627502, -2.551332950592041, 0.41171061992645264
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2276943922042847, -0.04652608186006546, 2.7350475788116455
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(849 + frame)
obj = cameras['Camera']
obj.location = 0.899296760559082, -2.55391526222229, 0.41151511669158936
obj.scale = 1.000000238418579, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2270954847335815, -0.04643890634179115, 2.7309305667877197
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(850 + frame)
obj = cameras['Camera']
obj.location = 0.9033891558647156, -2.5563015937805176, 0.4105703830718994
obj.scale = 1.000000238418579, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2264553308486938, -0.04594588652253151, 2.7273998260498047
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(851 + frame)
obj = cameras['Camera']
obj.location = 0.9068734645843506, -2.559183120727539, 0.40939611196517944
obj.scale = 1.000000238418579, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2254838943481445, -0.0473778210580349, 2.724005699157715
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(852 + frame)
obj = cameras['Camera']
obj.location = 0.9115153551101685, -2.5616836547851562, 0.4083409309387207
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2245688438415527, -0.0492837131023407, 2.7210850715637207
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(853 + frame)
obj = cameras['Camera']
obj.location = 0.9162112474441528, -2.5637362003326416, 0.407554566860199
obj.scale = 1.000000238418579, 0.9999998807907104, 0.9999998211860657
obj.rotation_euler = 1.2234426736831665, -0.052299465984106064, 2.719189405441284
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(854 + frame)
obj = cameras['Camera']
obj.location = 0.9209529757499695, -2.566086769104004, 0.4070567786693573
obj.scale = 1.000000238418579, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2215996980667114, -0.055314477533102036, 2.7178421020507812
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(855 + frame)
obj = cameras['Camera']
obj.location = 0.9247210621833801, -2.5689988136291504, 0.40664732456207275
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2195117473602295, -0.05706373229622841, 2.7171478271484375
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(856 + frame)
obj = cameras['Camera']
obj.location = 0.9278256297111511, -2.572476863861084, 0.4057278037071228
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998211860657
obj.rotation_euler = 1.2178980112075806, -0.05651025474071503, 2.7160141468048096
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(857 + frame)
obj = cameras['Camera']
obj.location = 0.9313305020332336, -2.576474666595459, 0.4053606688976288
obj.scale = 1.000000238418579, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2168651819229126, -0.054926544427871704, 2.713831663131714
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(858 + frame)
obj = cameras['Camera']
obj.location = 0.9341155886650085, -2.580322504043579, 0.4049918055534363
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.216933250427246, -0.05067896097898483, 2.7109322547912598
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(859 + frame)
obj = cameras['Camera']
obj.location = 0.9382340908050537, -2.5843896865844727, 0.4034925699234009
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2183595895767212, -0.047051962465047836, 2.7068300247192383
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(860 + frame)
obj = cameras['Camera']
obj.location = 0.9422606825828552, -2.588189125061035, 0.40313032269477844
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2197914123535156, -0.043914034962654114, 2.7029337882995605
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(861 + frame)
obj = cameras['Camera']
obj.location = 0.9469916820526123, -2.592069387435913, 0.4018594026565552
obj.scale = 1.000000238418579, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2219266891479492, -0.0415782630443573, 2.6987855434417725
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(862 + frame)
obj = cameras['Camera']
obj.location = 0.9509130120277405, -2.596959114074707, 0.4005947709083557
obj.scale = 1.000000238418579, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2221347093582153, -0.044148191809654236, 2.6949105262756348
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(863 + frame)
obj = cameras['Camera']
obj.location = 0.958957314491272, -2.601642370223999, 0.40178900957107544
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2194938659667969, -0.04734736308455467, 2.688532829284668
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(864 + frame)
obj = cameras['Camera']
obj.location = 0.9633374214172363, -2.60463285446167, 0.4024733901023865
obj.scale = 1.000000238418579, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2172505855560303, -0.04727550968527794, 2.686338186264038
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(865 + frame)
obj = cameras['Camera']
obj.location = 0.967394232749939, -2.6067867279052734, 0.40330785512924194
obj.scale = 1.000000238418579, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2157323360443115, -0.04725515842437744, 2.685419797897339
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(866 + frame)
obj = cameras['Camera']
obj.location = 0.9720468521118164, -2.6083664894104004, 0.40435269474983215
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.214811086654663, -0.047110218554735184, 2.6849331855773926
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(867 + frame)
obj = cameras['Camera']
obj.location = 0.9759264588356018, -2.6116719245910645, 0.4037547707557678
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998211860657
obj.rotation_euler = 1.2146596908569336, -0.04777267947793007, 2.684551477432251
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(868 + frame)
obj = cameras['Camera']
obj.location = 0.9799863696098328, -2.616013526916504, 0.40251442790031433
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2145181894302368, -0.052743684500455856, 2.6838366985321045
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(869 + frame)
obj = cameras['Camera']
obj.location = 0.9815754294395447, -2.6183197498321533, 0.40337222814559937
obj.scale = 1.000000238418579, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2129229307174683, -0.06190299615263939, 2.6844241619110107
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(870 + frame)
obj = cameras['Camera']
obj.location = 0.9836175441741943, -2.6208677291870117, 0.40445196628570557
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2118524312973022, -0.06981341540813446, 2.679713726043701
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(871 + frame)
obj = cameras['Camera']
obj.location = 0.9883341789245605, -2.6246883869171143, 0.4056377708911896
obj.scale = 1.000000238418579, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2114206552505493, -0.07316548377275467, 2.672320604324341
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(872 + frame)
obj = cameras['Camera']
obj.location = 0.9931522607803345, -2.628502368927002, 0.40548190474510193
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2122180461883545, -0.07238086313009262, 2.666128635406494
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(873 + frame)
obj = cameras['Camera']
obj.location = 0.996538519859314, -2.6305923461914062, 0.4055722653865814
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.212760090827942, -0.07142263650894165, 2.6613595485687256
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(874 + frame)
obj = cameras['Camera']
obj.location = 0.9972659945487976, -2.63102650642395, 0.40554457902908325
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.213104486465454, -0.07229090481996536, 2.6545605659484863
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(875 + frame)
obj = cameras['Camera']
obj.location = 1.000137448310852, -2.6336655616760254, 0.4060801863670349
obj.scale = 1.000000238418579, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2128719091415405, -0.07311240583658218, 2.642177104949951
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(876 + frame)
obj = cameras['Camera']
obj.location = 1.0061745643615723, -2.6367833614349365, 0.40559613704681396
obj.scale = 1.000000238418579, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.213836431503296, -0.0717114806175232, 2.628281831741333
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(877 + frame)
obj = cameras['Camera']
obj.location = 1.0120517015457153, -2.6431210041046143, 0.4030182957649231
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2153865098953247, -0.0693080946803093, 2.6161022186279297
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(878 + frame)
obj = cameras['Camera']
obj.location = 1.0197865962982178, -2.6469507217407227, 0.40316152572631836
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2151328325271606, -0.06453686952590942, 2.6071808338165283
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(879 + frame)
obj = cameras['Camera']
obj.location = 1.0237805843353271, -2.6490554809570312, 0.4029351472854614
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2147536277770996, -0.05798901617527008, 2.601062774658203
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(880 + frame)
obj = cameras['Camera']
obj.location = 1.0253489017486572, -2.6495540142059326, 0.401540607213974
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.215490460395813, -0.0534256249666214, 2.5927255153656006
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(881 + frame)
obj = cameras['Camera']
obj.location = 1.0291662216186523, -2.651247501373291, 0.4022710919380188
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.214802622795105, -0.05213719606399536, 2.5805723667144775
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(882 + frame)
obj = cameras['Camera']
obj.location = 1.0351781845092773, -2.654430389404297, 0.40196293592453003
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2149978876113892, -0.05150916799902916, 2.5654337406158447
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(883 + frame)
obj = cameras['Camera']
obj.location = 1.0468254089355469, -2.6600875854492188, 0.40185871720314026
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2146095037460327, -0.05128234624862671, 2.549713611602783
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(884 + frame)
obj = cameras['Camera']
obj.location = 1.0568811893463135, -2.664383888244629, 0.40173256397247314
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.213344693183899, -0.050212059170007706, 2.541171073913574
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(885 + frame)
obj = cameras['Camera']
obj.location = 1.0635769367218018, -2.6668362617492676, 0.4011099934577942
obj.scale = 1.000000238418579, 0.9999998807907104, 0.9999998211860657
obj.rotation_euler = 1.211825966835022, -0.04966975376009941, 2.5370852947235107
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(886 + frame)
obj = cameras['Camera']
obj.location = 1.0680992603302002, -2.6667158603668213, 0.3953559994697571
obj.scale = 1.000000238418579, 0.9999999403953552, 0.9999998211860657
obj.rotation_euler = 1.2144922018051147, -0.04669113829731941, 2.534113883972168
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(887 + frame)
obj = cameras['Camera']
obj.location = 1.0733420848846436, -2.669802665710449, 0.39601653814315796
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2114194631576538, -0.04343932121992111, 2.5294528007507324
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(888 + frame)
obj = cameras['Camera']
obj.location = 1.0788354873657227, -2.6722655296325684, 0.39707255363464355
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2088656425476074, -0.040793851017951965, 2.525319814682007
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(889 + frame)
obj = cameras['Camera']
obj.location = 1.084333896636963, -2.674126148223877, 0.3980928063392639
obj.scale = 1.000000238418579, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2074191570281982, -0.037270687520504, 2.5212924480438232
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(890 + frame)
obj = cameras['Camera']
obj.location = 1.0849499702453613, -2.674882411956787, 0.39841216802597046
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2070109844207764, -0.033576007932424545, 2.517645835876465
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(891 + frame)
obj = cameras['Camera']
obj.location = 1.090179204940796, -2.6775119304656982, 0.39944660663604736
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2077642679214478, -0.034457843750715256, 2.5042824745178223
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(892 + frame)
obj = cameras['Camera']
obj.location = 1.0971145629882812, -2.6806986331939697, 0.3994414508342743
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999999403953552
obj.rotation_euler = 1.209900975227356, -0.03685092180967331, 2.495230197906494
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(893 + frame)
obj = cameras['Camera']
obj.location = 1.1030421257019043, -2.683164596557617, 0.3987189531326294
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2120156288146973, -0.04246117174625397, 2.4928319454193115
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(894 + frame)
obj = cameras['Camera']
obj.location = 1.1075615882873535, -2.6843175888061523, 0.3987286388874054
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.212799310684204, -0.049227822571992874, 2.493274450302124
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(895 + frame)
obj = cameras['Camera']
obj.location = 1.1116952896118164, -2.6861960887908936, 0.39889341592788696
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999998807907104
obj.rotation_euler = 1.2124741077423096, -0.052983786910772324, 2.4929888248443604
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(896 + frame)
obj = cameras['Camera']
obj.location = 1.116257667541504, -2.690345287322998, 0.3975817561149597
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2123688459396362, -0.05430489405989647, 2.4901633262634277
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(897 + frame)
obj = cameras['Camera']
obj.location = 1.1205384731292725, -2.6944761276245117, 0.39735567569732666
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.211887240409851, -0.054890308529138565, 2.4859580993652344
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(898 + frame)
obj = cameras['Camera']
obj.location = 1.124403953552246, -2.696943998336792, 0.3977445065975189
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999999403953552
obj.rotation_euler = 1.2114918231964111, -0.053359366953372955, 2.4826500415802
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(899 + frame)
obj = cameras['Camera']
obj.location = 1.1277780532836914, -2.699000358581543, 0.3978147506713867
obj.scale = 1.0000003576278687, 0.9999998807907104, 0.9999998807907104
obj.rotation_euler = 1.2113226652145386, -0.052348338067531586, 2.480522394180298
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# new frame
scene.frame_set(900 + frame)
obj = cameras['Camera']
obj.location = 1.1317002773284912, -2.700429916381836, 0.3976733386516571
obj.scale = 1.0000003576278687, 0.9999999403953552, 0.9999999403953552
obj.rotation_euler = 1.2110811471939087, -0.05442546308040619, 2.480060338973999
obj.keyframe_insert('location')
obj.keyframe_insert('scale')
obj.keyframe_insert('rotation_euler')
data = obj.data
data.lens = 30.60024642944336
data.keyframe_insert('lens')

# markers
