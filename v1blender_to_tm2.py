# One TM2Stadium Block is 32x32x8
# The Valley car is ~ 4x2.2x1.6
# The center of the map is at 512,512
# The floor is 9 high

import re, math

anchor_blender = {
    "pos": [0.54, -3.32, 0],
    "size": 0.45
}

anchor_trackmania = {
    "center": [500, 231.5, 18],
    "scale": 4
}


def transform_loc(loc):
    ret = [0, 0, 0]
    # We divide by 2 because cube sizes in blender are mesured as radius
    full_scale = anchor_trackmania["scale"] / anchor_blender["size"] / 2

    for i in range(3):
        offset_trackmania = (loc[i] - anchor_blender["pos"][i]) * full_scale
        if(i == 0):
            offset_trackmania *= -1
        ret[i] = offset_trackmania + anchor_trackmania["center"][i]
    return ret

data_stage_0 = []

with open("input/input.py", "r") as f:
    data_stage_0 = f.read().split("# ")

#This regex will match something that looks like:
#  new frame ...
#  obj.location = (coordinates)             <--- 1st group
#  obj.scale ... 
#  obj.rotation = (coordinates)             <--- 2nd group
#  ...
reg_ex=r'new frame.*obj\.location = ([\-\d\.\s,]*)\nobj\.scale.*obj\.rotation_euler = ([-\d\.\s,]*)\n.*'

with open("input/output.txt", "w") as out:
    for frame_data in data_stage_0:
        match = re.fullmatch(reg_ex, frame_data, re.S)
        if match != None:
            #get the coordinate triples and pack them in arrays
            loc = re.split(r",\s", match.group(1))
            rot = re.split(r",\s", match.group(2))

            #convert the strings into floats
            loc = list(map(float, loc))
            rot = list(map(float,rot))
            
            #we have to switch y and z because Maniaplanet... 
            loc = transform_loc(loc)
            loc[1], loc[2] = loc[2], loc[1]

            rot = list(map(lambda x: x / math.pi * 180, rot))
            rot[0] = 90 - rot[0]
            #and the same for the rotation
            rot[1], rot[2] = rot[2] % 360, rot[1]

            first_c = True
            for coord in loc + rot:
                if(first_c):
                    first_c = False
                    out.write(str(round(coord, 3)))
                else:
                    out.write(", " + str(round(coord, 3)))
            out.write("\n")